//
//  AppConstant.swift
//  MyParty
//
//  Created by ABC on 8/24/18.
//  Copyright © 2018 CreativeThoughtsInformatics. All rights reserved.
//

import Foundation


let appDelegate = UIApplication.shared.delegate as! AppDelegate
let loginDetail = "UserDefaultLoginDetail"
let AppDwonloadAppUrl = "https://itunes.apple.com/us/app/sporto-pick-up-sport-nearby/id1418489329?ls=1&mt=8"

//MARK:- Common Api Key
let UserLoggedIn                     = "UserLoggedIn"
let DeviceTokenKey                   = "ios_token"
let AndroidToken                     = "android_token"
let DeviceType                       = "device_type"
let latitude                         = "lat"
let longitiude                       = "lng"
let UserId                           = "user_id"
let FCMToken                         =  "fcmToken"
let DeviceTypeValue                  = "ios"
let HeaderKey                        = "Authorization"
let HeaderValue                      = "dfs#!df154$"
let DeviceTokenValue                 = "DeviceToken"


//************************** Constants for Alert messages **************************//
let Key_Alert                        = "PTE"
let Key_Message                      = "Message"
let networkAlertMessage              = "Please Check Internet Connection"

// ******** Api Constant ******

let success = "success"
let message = "message"
let code = "code"

let Free = "  Free  "
let TokenRequired = "  Token required  "

//MARK:- color define
struct AppColor {
    
    static var redColor = UIColor(red: 255/255.0, green: 94/255.0, blue: 94/255.0, alpha: 1)
    static var greenClr = UIColor(red: 64/255.0, green: 192/255.0, blue: 2/255.0, alpha: 1)
    
    static var darkTextColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
    static var lightTextColor = UIColor(red: 52/255, green: 123/255, blue: 145/255, alpha: 1.0)
    
    static var clr1     = UIColor(red:  32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1.0)
    static var clr2   = UIColor(red: 64/255.0, green: 192/255.0, blue:  2/255.0, alpha: 1.0)
    static var clr3   = UIColor(red: 255/255.0, green:  94/255.0, blue:  94/255.0, alpha: 1.0)
    static var clr4   = UIColor(red:  32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1.0)
}


struct ImageBaseUrl {
 static let questionImageUrl = "https://ptefilesbucket.s3-ap-southeast-2.amazonaws.com/test_question/0/"
    
}

//MARK:- App Constant
struct AppConstant {
    
    static var appName = "PTE Master"
    static var errorMsg = "Error"
    static var networkMessage = "Please Check Internet Connection"
    static var message = "message"
    static let couldNotConnect = "Could not able to connect with server. Please try again."
}




