//
//  NetworkHttpClient.swift
//  MyParty
//
//  Created by ABC on 04/09/18.
//  Copyright © 2018 CreativeThoughtsInformatics. All rights reserved.


import UIKit


let BaseUrl = "https://www.masterpte.com.au/api/"
let BaseUrlTestImg = "https://www.masterpte.com.au/assets/test/"
let BaseUrlTestQuestion = "https://ptefilesbucket.s3.amazonaws.com/test_question/"
let BaseUrlProfileImg = "https://ptefilesbucket.s3.amazonaws.com/userprofile/"

// API paths.
public enum RequestPath: String {
    
    // final api
    case LoginApi            = "login"
    case SignupApi           = "signup"
    case forgetPasswordApi   = "forgetPassword?"
    case DashboradApi        = "dashboard"
    case MockTestDetailApi   = "testDetails?"
    case TestPromoCodeCkeck  = "testPromoCheck?"
    case subscribeTest       = "buynow?"
    case getTestApi          = "gettest"
    case sepakingResult = "add_speaking_result"
    case writingResult  = "add_result_writing"
    case listningResult = "add_result_listening"
    case readingResult = "add_result_reading"
    case practiceCategoryApi   = "PracticeSections"
    case practiceTestApi   = "PracticeQuestion"
    case testHistoryList = "testHistory"
    case reportHistory   = "report_history"
    case attemptTest = "testAttempt"
    case updateProfileImage = "updateprofile"
    case practice_question = "practice_question"
    case token_packs = "token_packs"
    case account_details = "account_details"
    case token_buynow = "token_buynow?"
    case practice_speaking_mobile = "practice_speaking_mobile"
    case PracticeQuestionNew = "PracticeQuestionNew"
    //hp
    case practice_result_writing = "practice_result_writing"
    
    
    
}

class NetworkHttpClient: NSObject {
    
    static func callAPIWith(_ apiName: String, parameter: [String: Any] = [:], showHud: Bool = true, completionHandler: @escaping (Bool, JSON?, String?, Error?) -> Void) {
        
        if appDelegate.isNetworkAvailable {
            self.showHUD(true, showHUD: showHud)
            

           // let headers: HTTPHeaders = ["Content-type": "multipart/form-data","Authorization": HeaderValue]
            
            let urlString = BaseUrl + apiName
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameter {                    
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: nil) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.showHUD(false, showHUD: showHud)
                        do {
                            DispatchQueue.main.async {
                                if let value = response.result.value {
                                    let responseJson = JSON(value)
                                    if responseJson["status"].string == "1" {
                                        completionHandler(true, responseJson, responseJson["msg"].string, nil)
                                    } else if responseJson ["code"].string != "1" {
                                        completionHandler(false, responseJson, responseJson["msg"].string, nil)
                                    }
                                } else {
                                    completionHandler(false, nil, AppConstant.couldNotConnect, response.result.error)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    self.showHUD(false, showHUD: showHud)
                    print("Error in upload: \(error.localizedDescription)")
                    completionHandler(false, nil, error.localizedDescription, error)
                }
            }
        } else {
            Util.showAlertWithCallback(AppConstant.appName, message: networkAlertMessage, isWithCancel: false)
        }
    }
    
    static func callAPIWithImage(_ apiName: String, parameter: [String: Any] = [:], _ imageData : Data, showHud: Bool = true, completionHandler: @escaping (Bool, JSON?, String?, Error?) -> Void) {
        
        if appDelegate.isNetworkAvailable {
            self.showHUD(true, showHUD: showHud)
            
            
            // let headers: HTTPHeaders = ["Content-type": "multipart/form-data","Authorization": HeaderValue]
            
            let urlString = BaseUrl + apiName
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if (imageData.count > 0)
                {
                    multipartFormData.append(imageData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                }
                else
                {
                    
                }
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: nil) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.showHUD(false, showHUD: showHud)
                        do {
                            DispatchQueue.main.async {
                                if let value = response.result.value {
                                    let responseJson = JSON(value)
                                    if responseJson["status"].string == "1" {
                                        completionHandler(true, responseJson, responseJson["msg"].string, nil)
                                    } else if responseJson ["code"].string != "1" {
                                        completionHandler(false, responseJson, responseJson["msg"].string, nil)
                                    }
                                } else {
                                    completionHandler(false, nil, AppConstant.couldNotConnect, response.result.error)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    self.showHUD(false, showHUD: showHud)
                    print("Error in upload: \(error.localizedDescription)")
                    completionHandler(false, nil, error.localizedDescription, error)
                }
            }
        } else {
            Util.showAlertWithCallback(AppConstant.appName, message: networkAlertMessage, isWithCancel: false)
        }
    }
    
//    static func callAPIWithImage(_ apiName: String, _ imageData : Data, parameter: [String: Any] = [:], completionHandler: @escaping (Bool, JSON?, String?, Error?) -> Void)
//    {
//        if appDelegate.isNetworkAvailable {
//            self.showHUD(false, showHUD : showHUD)
//
//            let headers: HTTPHeaders = ["Content-type": "multipart/form-data","Authorization": HeaderValue]
//
//            let urlString = BaseUrl + apiName
//
//            Alamofire.upload(multipartFormData: { (multipartFormData) in
//
//                for (key, value) in parameter
//                {
//                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//                }
//                if (imageData.count > 0)
//                {
//                    multipartFormData.append(imageData, withName: "profile_image",fileName: "file.jpg", mimeType: "image/jpg")
//                }
//                else
//                {
//
//                }
//
//            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
//
//                switch result
//                {
//                case .success(let upload, _, _):
//                    upload.responseJSON { response in
//                        // ShowLoder()
//
//                        do
//                        {
//                            DispatchQueue.main.async
//                                {
//                                    if let value = response.result.value
//                                    {
//                                        let responseJson = JSON(value)
//                                        if responseJson["success"].string == "1"
//                                        {
//                                            completionHandler(true, responseJson, responseJson["message"].string, nil)
//                                        }
//                                        else if responseJson ["code"].string != "1"
//                                        {
//                                            completionHandler(false, responseJson, responseJson["message"].string, nil)
//                                        }
//                                    }
//                                    else
//                                    {
//                                        completionHandler(false, nil, AppConstant.couldNotConnect, response.result.error)
//                                    }
//                            }
//                        }
//                    }
//
//                case .failure(let error):
//                    //HideLoder()
//                    print("Error in upload: \(error.localizedDescription)")
//                    completionHandler(false, nil, error.localizedDescription, error)
//                }
//            }
//        }
//        else
//        {
//           Util.showAlertWithCallback(AppConstant.appName, message: networkAlertMessage, isWithCancel: false)
//        }
//    }
    
    static func showHUD(_ status: Bool, showHUD: Bool) {
        if showHUD {
//           DispatchQueue.main.async {
                //status ? Loader.showLoader("Loading...") : Loader.hideLoader()
//            }
        }
    }
    
}
