//
//  HistoryListModel.swift
//  PTEMaster
//
//  Created by mac on 16/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation


struct HistoryListModel {
    
    var id : String?
    var userId : String?
    var testId : String?
    var completedStatut : String?
    var purchaseToken : String?
    var purchaseStatus : String?
    var promo : String?
    var startTime : String?
    var terminateTime : String?
    var timeDifference : String?
    var testQuestionNumber : String?
    var buyDate : String?
    var testTime : String?
    var speakingSectionTimer : String?
    var readingSectionTimer : String?
    var listeningSectionTimer : String?
    var details : HistoryDetailListModel?

    init(_ dict: [String: JSON]) {
        
        id = dict["id"]?.stringValue
        userId = dict["user_id"]?.stringValue
        testId = dict["test_id"]?.stringValue
        completedStatut = dict["completed_statut"]?.stringValue
        purchaseToken = dict["purchase_token"]?.stringValue
        purchaseStatus = dict["purchase_status"]?.stringValue
        promo = dict["promo"]?.stringValue
        startTime = dict["start_time"]?.stringValue
        terminateTime = dict["terminat_time"]?.stringValue
        timeDifference = dict["time_defference"]?.stringValue
        testQuestionNumber = dict["test_question_number"]?.stringValue
        buyDate = dict["buy_date"]?.stringValue
        testTime = dict["test_time"]?.stringValue
        speakingSectionTimer = dict["speaking_section_timer"]?.stringValue
        readingSectionTimer = dict["reading_section_timer"]?.stringValue
        listeningSectionTimer = dict["listening_section_timer"]?.stringValue
    
        if let detail = dict["details"]?.dictionary {
            self.details = HistoryDetailListModel.init(detail)
        }
    
    }
    
    
    init(json: JSON) {
        
        id = json["id"].stringValue
        userId = json["user_id"].stringValue
        testId = json["test_id"].stringValue
        completedStatut = json["completed_statut"].stringValue
        purchaseToken = json["purchase_token"].stringValue
        purchaseStatus = json["purchase_status"].stringValue
        promo = json["promo"].stringValue
        startTime = json["start_time"].stringValue
        terminateTime = json["terminat_time"].stringValue
        timeDifference = json["time_defference"].stringValue
        testQuestionNumber = json["test_question_number"].stringValue
        buyDate = json["buy_date"].stringValue
        testTime = json["test_time"].stringValue
        speakingSectionTimer = json["speaking_section_timer"].stringValue
        readingSectionTimer = json["reading_section_timer"].stringValue
        listeningSectionTimer = json["listening_section_timer"].stringValue
        
        if let detail = json["details"].dictionary {
            self.details = HistoryDetailListModel.init(detail)
        }
        
    }
    
}

struct HistoryDetailListModel {
    
    var id : String?
    var name : String?
    var shortDiscription : String?
    var image : String?
    var description : String?
    var createdAt : String?
    var duration : String?
    var price : String?
    var discount : String?
    var durationInFormate: String?
    var questionNumber: String?

    init(_ dict: [String: JSON]) {
        
        id = dict["id"]?.stringValue
        name = dict["name"]?.stringValue
        shortDiscription = dict["short_desc"]?.stringValue
        description = dict["description"]?.stringValue
        image = dict["image"]?.stringValue
        createdAt = dict["created_at"]?.stringValue
        duration = dict["duration"]?.stringValue
        price = dict["price"]?.stringValue
        discount = dict["discount"]?.stringValue
        questionNumber = dict["question_number"]?.stringValue
    
        if let sDate = duration {
            let timeInSec = sDate.convertServerDateToTimeInterval("HH:mm:ss")
            durationInFormate = Date(timeIntervalSince1970: timeInSec).convertDateToLocalString(withFormat: "HH:mm")
            
        }
       
    }

    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        shortDiscription = json["short_desc"].stringValue
        description = json["description"].stringValue
        image = json["image"].stringValue
        createdAt = json["created_at"].stringValue
        duration = json["duration"].stringValue
        price = json["price"].stringValue
        discount = json["discount"].stringValue
        questionNumber = json["question_number"].stringValue

        if let sDate = duration {
            let timeInSec = sDate.convertServerDateToTimeInterval("HH:mm:ss")
            durationInFormate = Date(timeIntervalSince1970: timeInSec).convertDateToLocalString(withFormat: "HH:mm")
            
        }
        
    }
    
}

