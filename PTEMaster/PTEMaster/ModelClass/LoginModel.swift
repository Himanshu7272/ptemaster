//
//  LoginModel.swift
//  MyParty
//
//  Created by ABC on 9/5/18.
//  Copyright © 2018 CreativeThoughtsInformatics. All rights reserved.
//

import Foundation

struct LoginModel
{
    
    var id: Int!
    var name: String?
    var email: String?
    var mobile: String?
    var image: String?
    var token: String?
    var actToken: String?
    var social: String?
    var createdAt: String?
    var status: String?
    var subscription: String?
    var course: String?
    var score: String?
    var ptestatus: String?
    
   
    init(loginDetail: [String: Any]) {
        
        id = loginDetail.getInt(forKey: "id")
        name = loginDetail.getString(forKey: "name")
        email = loginDetail.getString(forKey: "email")
        mobile = loginDetail.getString(forKey: "mobile")
        image = loginDetail.getString(forKey: "image")
        token = loginDetail.getString(forKey: "token")
        actToken = loginDetail.getString(forKey: "act_token")
        social = loginDetail.getString(forKey: "social")
        createdAt = loginDetail.getString(forKey: "created_at")
        status = loginDetail.getString(forKey: "status")
        subscription = loginDetail.getString(forKey: "subscription")
        course = loginDetail.getString(forKey: "course")
        ptestatus = loginDetail.getString(forKey: "ptestatus")
        score = loginDetail.getString(forKey: "score")
        
        //updateProviderDisplayTimeData()
        
    }
    
    
    init(_ dict: [String: JSON]) {
        
        //userId = dict["id"]?.intValue // if not optional
        //firstName = dict["first_name"]?.stringValue // optional
        
        id = dict["id"]?.intValue
        name = dict["name"]?.stringValue
        email = dict["email"]?.stringValue
        mobile = dict["mobile"]?.stringValue
        image = dict["image"]?.stringValue
        token = dict["token"]?.stringValue
        actToken = dict["act_token"]?.stringValue
        social = dict["social"]?.stringValue
        createdAt = dict["created_at"]?.stringValue
        status = dict["status"]?.stringValue
        subscription = dict["subscription"]?.stringValue
        course = dict["course"]?.stringValue
        ptestatus = dict["ptestatus"]?.stringValue
        score = dict["score"]?.stringValue
    

    }

    func getDictionary() -> [String: Any] {
        var dict = [String: Any]()
        
        dict["id"] = id
        dict["name"] = name.validate
        dict["email"] = email.validate
        dict["mobile"] = mobile.validate
        dict["token"] = token.validate
        dict["act_token"]    = actToken.validate
        dict["image"] = image.validate
        dict["social"] = social.validate
        dict["created_at"] = createdAt.validate
        dict["status"] = status.validate
        dict["subscription"] = subscription.validate
        dict["course"] = course.validate
        dict["ptestatus"] = ptestatus.validate
        dict["score"] = score.validate
       

        return dict
    }
    
    
   
    
}

extension Optional where Wrapped == String {
    
    var isBlank: Bool {
        return self.validate.count == 0
    }
    
    var validate: String {
        return self ?? ""
    }
    
}
