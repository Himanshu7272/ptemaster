//
//  TokenModel.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation


class TokenModal: NSObject {
    
    var tk_pkg_from_date:String?
    var deals:String?
    var inhouse:String?
    var tk_pkg_credits:String?
    var tk_pkg_name:String?
    var tk_pkg_description:String?
    var consume:String?
    var tk_pkg_id:String?
    var tk_pkg_active_time:String?
    var tk_pkg_to_date:String?
    var tk_pkg_type:String?
    var tk_pkg_price:String?
    var tk_pkg_discount:String?
    
    init(parameters:JSON) {
        tk_pkg_from_date = parameters["tk_pkg_from_date"].stringValue
        deals = parameters["deals"].stringValue
        inhouse  = parameters["inhouse"].stringValue
        tk_pkg_credits = parameters["tk_pkg_credits"].stringValue
        tk_pkg_name = parameters["tk_pkg_name"].stringValue
        tk_pkg_description = parameters["tk_pkg_description"].stringValue
        consume = parameters["consume"].stringValue
        tk_pkg_id = parameters["tk_pkg_id"].stringValue
        tk_pkg_active_time = parameters["tk_pkg_active_time"].stringValue
        tk_pkg_to_date = parameters["tk_pkg_to_date"].stringValue
        tk_pkg_type = parameters["tk_pkg_type"].stringValue
        tk_pkg_price = parameters["tk_pkg_price"].stringValue
        tk_pkg_discount = parameters["tk_pkg_discount"].stringValue
    }
}


