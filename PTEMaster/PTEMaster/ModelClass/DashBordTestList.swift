//
//  DashBordTestList.swift
//  PTEMaster
//
//  Created by mac on 27/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

struct DashBordTestListModel {

    var created_at : String?
    var descriptionVal : String?
    var discount : String?
    var duration : String?
    var id : String?
    var image : String?
    var name : String?
    var price : String?
    var question_number : String?
    var short_desc : String?
    var status : String?
    
    
    init(_ dict: [String: JSON]) {
        
        created_at = dict["created_at"]?.stringValue
        descriptionVal = dict["description"]?.stringValue
        id = dict["id"]?.stringValue
        discount = dict["discount"]?.stringValue
        duration = dict["duration"]?.stringValue
        image = dict["image"]?.stringValue
        name = dict["name"]?.stringValue
        question_number = dict["question_number"]?.stringValue
        short_desc = dict["short_desc"]?.stringValue
        status = dict["state"]?.stringValue
        price = dict["price"]?.stringValue
    }
    
    
    init(json: JSON) {
        
        created_at = json["created_at"].stringValue
        descriptionVal = json["description"].stringValue
        id = json["id"].stringValue
        discount = json["discount"].stringValue
        duration = json["duration"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        question_number = json["question_number"].stringValue
        short_desc = json["short_desc"].stringValue
        status = json["state"].stringValue
        price = json["price"].stringValue
        
    }

}
