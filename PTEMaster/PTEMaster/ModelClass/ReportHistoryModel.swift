//
//  ReportHistoryModel.swift
//  PTEMaster
//
//  Created by mac on 18/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

struct ReportHistoryModel {
    
    var scoreVocab: Double?
    var scoreGrammer : Double?
    var scoreSpelling : Double?
    var scorePronunciation : Double?
    var scoreFluency : Double?
    var scoreForm : Double?
    var scoreSpeakinigSection : Double?
    var scoreListeningSection : Double?
    var scoreWritingSection : Double?
    var scoreReadingSection: Double?
    
    init(_ dict: [String: JSON]) {
        scoreVocab = dict["score_vocab"]?.doubleValue
        scoreGrammer = dict["score_grammar"]?.doubleValue
        scoreSpelling = dict["score_spelling"]?.doubleValue
        scorePronunciation = dict["score_pronunciation"]?.doubleValue
        scoreFluency = dict["score_fluency"]?.doubleValue
        scoreForm = dict["score_form"]?.doubleValue
        scoreSpeakinigSection = dict["score_speakinig_section"]?.doubleValue
        scoreListeningSection = dict["score_listening_section"]?.doubleValue
        scoreWritingSection = dict["score_writing_section"]?.doubleValue
        scoreReadingSection = dict["score_reading_section"]?.doubleValue
    }
    
    init(json: JSON) {
        scoreVocab = json["score_vocab"].doubleValue
        scoreGrammer = json["score_grammar"].doubleValue
        scoreSpelling = json["score_spelling"].doubleValue
        scorePronunciation = json["score_pronunciation"].doubleValue
        scoreFluency = json["score_fluency"].doubleValue
        scoreForm = json["score_form"].doubleValue
        scoreSpeakinigSection = json["score_speakinig_section"].doubleValue
        scoreListeningSection = json["score_listening_section"].doubleValue
        scoreWritingSection = json["score_writing_section"].doubleValue
        scoreReadingSection = json["score_reading_section"].doubleValue
    }
}


struct ScoreDetail {
    var name: String
    var score: Double
    var scoreColor: UIColor
}
