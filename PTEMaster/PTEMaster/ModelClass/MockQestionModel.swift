//
//  MockQestionModel.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 24/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

struct MockQestionModel {
    
    var id: String?
    var categoryId: String?
    var testId: String?
    var shortTitle: String?
    var description: String?
    var audio: String?
    var image: String?
    var question: String?
    var categoryName: String?
    var subCategoryName: String?
    var questionNumber: Int!
    var questionId: String?
    var totalTimeRemaining: TimeInterval!
    var totalQuestion: Int!
    var waitTime: TimeInterval!
    var responseTime: TimeInterval!
    var categoryType: CategoryType!
    
    
    init(json: JSON) {
        id = json["id"].string
        categoryId = json["category_id"].string
        testId = json["test_id"].string
        shortTitle = json["short_title"].string
        description = json["description"].string
        audio = json["audio"].string
        image = json["image"].string
        question = json["question"].string
        categoryName = json["category_name"].string
        subCategoryName = json["sub_category_name"].string
        questionNumber = json["question_number"].intValue
        questionId = json["question_id"].string
        totalTimeRemaining = json["total_time_remaining"].doubleValue
        totalQuestion = json["total_question"].intValue
        waitTime = json["wait_time"].doubleValue
        responseTime = json["response_time"].doubleValue
        categoryType = CategoryType(rawValue: categoryId ?? "") ?? CategoryType.summarizeWrittenText
        
    }
    
}


struct OptionsModel {
    
    var id: String?
    var option: String?
    var correct: String?
    var sequence: String?
    var isSelected = false
    
    init(json: JSON) {
        id = json["id"].string
        option = json["options"].string
        correct = json["correct"].string
        sequence = json["sequence"].string
        isSelected = false
    }

}


enum CategoryType: String {
    
   // speaking
    case readAloud = "5"
    case repeatSentence = "6"
    case describeImg = "7"
    case reTellLecture = "8"
    case ansShortQuestn = "9"
    
    // writing
    case summarizeWrittenText = "10"
    case writingEasy = "11"
    
    // reading
    case singleChoice = "12"
    case multipleAnswere = "13"
    case reOrderParagraph   = "14"
    case readFillInBlanks = "15"
    case readWriteBlanks = "16"
    
    // listening 
    case listSummarySpokenText = "17"
    case listMultipleChoiceMultipleAns = "18"
    case listFillInTheBlanks  = "19"
    case listHighlightCorrectSummery   = "20"
    case listMultipleChoiceSingleAns = "21"
    case listSelectMissingWord = "22"
    case listHighlightInCorrectWord = "23"
    case listWriteFromDictation = "24"
    

}
