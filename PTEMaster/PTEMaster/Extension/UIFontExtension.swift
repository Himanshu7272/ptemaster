//
//  UIFontExtension.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 02/05/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func glacialIndifferenceRegular(of size: CGFloat) -> UIFont {
        return UIFont(name: "GlacialIndifference-Regular", size: size)!
    }
    
    static func glacialIndifferenceBold(of size: CGFloat) -> UIFont {
        return UIFont(name: "GlacialIndifference-Bold", size: size)!
    }
    
}
