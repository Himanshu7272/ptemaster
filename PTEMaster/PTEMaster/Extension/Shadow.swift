//
//  Shadow.swift
//  Radds
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class Shadow: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    override func layoutSubviews() {
        self.setup()
    }
    
    func setup() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
       // self.layer.cornerRadius = 0.0
    }

}
