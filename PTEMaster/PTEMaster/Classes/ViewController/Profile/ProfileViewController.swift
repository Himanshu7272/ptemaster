//
//  ProfileViewController.swift
//  PTEMaster
//
//  Created by mac on 13/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    var categoryName: [String] = ["ATTEMPT TEST", "COMPLETED", "TOTAL TEST"]
    var arr_TitleName: [String] = ["Settings" , "Rate App" , "Feedback" , "Support" , "Log out"]
    var arr_IconName: [String] = ["setting_icon" , "rate_icon","feedback_icon","help_icon" , "logout_icon"]
    
    @IBOutlet weak var collectionView_test: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var imageData = UIImage().jpegData(compressionQuality: 0)

    
    override func viewDidLoad(){
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.estimatedRowHeight = 55
        tableView.rowHeight = UITableView.automaticDimension
    }

    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setData()
    }
    
    func setData() {
        
        self.lblEmail.text = AppDataManager.shared.loginData.email.validate == "" ? "N/A" : AppDataManager.shared.loginData.email.validate
        
        self.lblUserName.text = AppDataManager.shared.loginData.name.validate == "" ? "N/A" : AppDataManager.shared.loginData.name.validate.capitalized
    
         self.lblNumber.text = AppDataManager.shared.loginData.mobile.validate == "" ? "N/A" : AppDataManager.shared.loginData.mobile.validate
        
        imgProfile.image = #imageLiteral(resourceName: "app_icon")
    
        if (AppDataManager.shared.loginData.image! == "")
        {
            imgProfile.image = #imageLiteral(resourceName: "profile-img")
        }
        else
        {
            if let url = URL(string: BaseUrlProfileImg+AppDataManager.shared.loginData.image!) {
         
                imgProfile.kf.indicatorType = .activity
         
              /*  imgProfile.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
                    if let error = error {
                        print("Error: \(error)")
                    } else if let image = image {
                        print("Image: \(image). Got from: \(cacheType)")
                    }
                }*/
         
                imgProfile.kf.setImage(with: url) { result in
                    switch result {
                    case .success(let value):
                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            }
        }
    }
    
    
    
    //MARK:- Button Action
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // not showing
    @IBAction func doclickonEditProfile(_ sender: Any) {
       // let vc = EditProfileViewController.instance()
       // self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnEditProfileImage(_ sender: Any) {
        
        print("change update")
        Util.openImagePicker()
    }
    
    //MARK: - IBAction
    
    func updateProfile()
    {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: AnyObject] =
                [ "name" : lblUserName.text as AnyObject,
                  "mobile" : lblNumber.text as AnyObject,
                  "user_id": AppDataManager.shared.loginData.id as AnyObject]
            
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if ((self.imageData!.count) > 0)
                {
                    multipartFormData.append(self.imageData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                }
                else
                {
                    
                }
                
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.updateProfileImage.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(loginDict)")
                                        
                                        let loginDataModel = LoginModel(loginDict)
                                        AppDataManager.shared.loginData = loginDataModel
                                        UserDefaults.standard.set(loginDataModel.getDictionary(), forKey: loginDetail)
                                        
                                        self.setData()

                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }

    }
    
}


//MARK:- Collection View Delegate

extension ProfileViewController:  UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryName.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:ProfileCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCollectionViewCell
        
        if indexPath.row == 1 {
            cell.lblNumberTest.textColor =  AppColor.greenClr
            cell.lblFooter.backgroundColor = AppColor.greenClr
        }else if indexPath.row == 2 {
            cell.lblNumberTest.textColor   = AppColor.redColor
            cell.lblFooter.backgroundColor = AppColor.redColor
        }
        
        cell.lblTaskName.text = categoryName[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.view.frame.width/3-20,height:85)
    }
}

//MARK:- table View Delegate
extension ProfileViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_TitleName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileTableViewCell
        
        cell.lblTitle.text = arr_TitleName[indexPath.row]
        cell.img_icon.image = UIImage(named: arr_IconName[indexPath.row])
        
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 4 {
            UserDefaults.standard.removeObject(forKey: loginDetail)
            self.navigationController!.setViewControllers([LoginViewController.instance()], animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    
}

//MARK:- image picker View Delegate
extension ProfileViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
     let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imgProfile.contentMode = .scaleAspectFill
            imgProfile.image = pickedImage
        }
        
        self.imageData = imgProfile.image!.jpegData(compressionQuality: 1)
        
        updateProfile()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("")
        self.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
