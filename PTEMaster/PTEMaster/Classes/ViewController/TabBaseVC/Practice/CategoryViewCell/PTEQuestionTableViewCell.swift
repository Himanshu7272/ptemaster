//
//  PTEQuestionTableViewCell.swift
//  PTEMaster
//
//  Created by mac on 21/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTEQuestionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblQuestionsCounts: UILabel!
     @IBOutlet weak var lblQuestions: UILabel!
     @IBOutlet weak var lblQuestionsNumber: UILabel!
     @IBOutlet weak var lblLines: UILabel!
   @IBOutlet weak var progressView: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(questions:PracticeQestionModel , index:Int) {
        switch questions.categoryId {
        case "10", "11":
            let questions = questions.question?.html2String
            lblQuestions.text = questions
        default:
            lblQuestions.text = questions.description
        }
        
        
        lblQuestionsNumber.text = "#\(questions.id ?? "0")"
        lblQuestionsCounts.text = "\(index + 1)"
        lblQuestionsCounts.clipsToBounds = true
        lblQuestionsCounts.layer.cornerRadius = lblQuestionsCounts.frame.size.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
