//
//  PracticeCategoryCell.swift
//  PTEMaster
//
//  Created by mac on 16/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class PracticeCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRequest: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewOuter.dropShadowAllSide()
        lblRequest.dropShadowAllSide()
    }

}
