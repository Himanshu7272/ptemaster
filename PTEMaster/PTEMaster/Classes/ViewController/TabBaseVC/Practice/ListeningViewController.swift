//
//  ListeningViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 20/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class ListeningViewController: UIViewController
{
    
   @IBOutlet weak var cvListning: UICollectionView!
    var category: PracticeCategoryModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          cvListning.register(UINib(nibName: "PracticeCategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PracticeCategoryCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       
    }

    func setCategoryDataAndUpdate(_ category: PracticeCategoryModel) {
        self.category = category
        if cvListning != nil {
            cvListning.reloadData()
        }
    }
}

/* extension ListeningViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category?.subCategory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView_listening.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileTableViewCell
        if let subCategory = category?.subCategory {
            cell.lblTitle.text = subCategory[indexPath.row].category
        }
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let practiceDetailVC = PracticeDetailVC.instance() as! PracticeDetailVC
        practiceDetailVC.currentRow = 0
        
        if let subCategory = category?.subCategory {
            practiceDetailVC.categoryId = "\(subCategory[indexPath.row].id!)"
        }
        
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
    }
    
} */

extension ListeningViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category?.subCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeCategoryCell", for: indexPath) as! PracticeCategoryCell
        
        if let subCategory = category?.subCategory {
            cell.lblTitle.text = subCategory[indexPath.row].category
            cell.viewOuter.borderColor = AppColor.clr1
            cell.viewOuter.borderWidth = 1.0
            cell.viewOuter.layer.cornerRadius = 5.0
            cell.lblRequest.clipsToBounds = true
            cell.lblRequest.layer.cornerRadius = 10
            cell.lblRequest.text = Free
            
            if subCategory[indexPath.row].id == 17 {
                cell.lblRequest.text = TokenRequired
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_summarize_text")
            }else if subCategory[indexPath.row].id == 18 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_multi_answer")
            }else if subCategory[indexPath.row].id == 19 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_fill_blanks")
            }else if subCategory[indexPath.row].id == 20 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_highlight_summary")
            }else if subCategory[indexPath.row].id == 21 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening-single_answer")
            }else if subCategory[indexPath.row].id == 22 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_highlight_summary")
            }else if subCategory[indexPath.row].id == 23 {
                cell.imgCategory.image = #imageLiteral(resourceName: "listening_highlight_summary")
            }else if subCategory[indexPath.row].id == 24 {
                cell.imgCategory.image = #imageLiteral(resourceName: "write_dictation")
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (cvListning.frame.width / 2)  - 0.5
        return CGSize(width:width , height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let practiceDetailVC = PTESortViewController.instance() as! PTESortViewController
        let subCategory = category?.subCategory
        practiceDetailVC.titles = subCategory![indexPath.row].category
        practiceDetailVC.categoryId = "\(subCategory![indexPath.row].id!)"
        practiceDetailVC.currentRow = indexPath.row
        staticCategory = category?.category
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
    }
}
