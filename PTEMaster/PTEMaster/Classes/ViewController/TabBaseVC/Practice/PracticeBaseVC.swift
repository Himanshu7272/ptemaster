//
//  PracticeBaseVC.swift
//  PTEMaster
//
//  Created by mac on 15/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class PracticeBaseVC: UIViewController {

    @IBOutlet weak var collectionVeiw: UICollectionView!
    
    var propertyDetailPageVC: PracticePageVC?
    var sections = [PracticSectionModel]()
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setScreenLayout()
    }
    
    func setScreenLayout() {
        sections.append(PracticSectionModel(title: "Speaking", selectedImage: "speaking_icon", unSelectedImage: "speaking_big_icon"))
        sections.append(PracticSectionModel(title: "Writing", selectedImage: "writing_icon", unSelectedImage: "writing_big_icon"))
        sections.append(PracticSectionModel(title: "Reading", selectedImage: "reading_icon", unSelectedImage: "reading_big_icon"))
        sections.append(PracticSectionModel(title: "Listening", selectedImage: "listening_icon", unSelectedImage: "listening_big_icon"))
        collectionVeiw.reloadData()
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PracticePageVC {
            propertyDetailPageVC = viewController
            viewController.delegateProperty = self
        }
    }
 
}

extension PracticeBaseVC: PracticePageVCProtocol {
    
    func updateSelectedCell(pageIndex: Int) {
        selectedIndex = pageIndex
        collectionVeiw.reloadData()
        collectionVeiw.scrollToItem(at: IndexPath(row: pageIndex, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
}



extension PracticeBaseVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeSectionCell", for: indexPath) as! PracticeSectionCell
        cell.viewIndicator.isHidden = selectedIndex != indexPath.item
        cell.lblTitle.text = sections[indexPath.row].title
        cell.lblTitle.textColor = selectedIndex == indexPath.item ? cell.viewIndicator.backgroundColor : UIColor.darkGray
        let imageName = selectedIndex == indexPath.item ? sections[indexPath.row].selectedImage : sections[indexPath.row].unSelectedImage
        cell.imgView.image = UIImage(named: imageName)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: collectionView.frame.size.height)
        if sections.indices.contains(indexPath.row) {
            size.width = sections[indexPath.row].titleWidth + 12 + 12 + 5 + 18
            // left padding + right padding + distance between image and label + image width
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionVeiw.reloadData()
       self.propertyDetailPageVC?.setCurrentIndex(index: indexPath.item)
        collectionVeiw.scrollToItem(at: IndexPath(row: indexPath.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
}



struct PracticSectionModel {
    var title: String {
        didSet {
            self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        }
    }
    var selectedImage: String
    var unSelectedImage: String
    var titleWidth: CGFloat
}


extension PracticSectionModel {
    
    init(title: String, selectedImage: String, unSelectedImage: String) {
        self.title = title
        self.selectedImage = selectedImage
        self.unSelectedImage = unSelectedImage
        self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
    }
    
}
