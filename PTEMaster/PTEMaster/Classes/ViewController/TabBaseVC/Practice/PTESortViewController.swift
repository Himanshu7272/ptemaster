//
//  PTESortViewController.swift
//  PTEMaster
//
//  Created by mac on 21/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTESortViewController: UIViewController {
    @IBOutlet weak var navigationCustoms: UINavigationBar!
    @IBOutlet weak var btnMP: UIButton!
    @IBOutlet weak var btnDN: UIButton!
    @IBOutlet weak var btnNQF: UIButton!
    @IBOutlet weak var btnHRQ: UIButton!
    var currentRow : Int = 0
    var categoryId = ""
    var checkStatus:Bool = true
    var titles:String?
    var questionType:Int = 0
    
    @IBOutlet weak var navigationItems: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItems.title = titles
        btnMP?.isSelected = true
        btnDN?.isSelected = false
        btnNQF?.isSelected = false
        btnHRQ?.isSelected = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBarButtonItems(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 205:
            if checkStatus {
                let vc = PTEQuestionsListViewController.instance() as! PTEQuestionsListViewController
                vc.categoryId = categoryId
                vc.currentRow = currentRow
                vc.titles = navigationItems.title
                vc.questionType = questionType
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                Util.showAlertWithMessage("Please select options", title: AppConstant.appName)
            }
        case 206:
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
    
    
    @IBAction func actionSorts(_ sender: UIButton) {
        checkStatus = true
        switch sender.tag {
        case 201:
            questionType = 0
            btnMP?.isSelected = true
            btnDN?.isSelected = false
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = false
        case 202:
            questionType = 1
            btnMP?.isSelected = false
            btnDN?.isSelected = true
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = false
        case 203:
            questionType = 2
            btnMP?.isSelected = false
            btnDN?.isSelected = false
            btnNQF?.isSelected = true
            btnHRQ?.isSelected = false
        case 204:
            questionType = 3
            btnMP?.isSelected = false
            btnDN?.isSelected = false
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = true
        default:
            break
        }
    }
    
 
    
    
    
}
