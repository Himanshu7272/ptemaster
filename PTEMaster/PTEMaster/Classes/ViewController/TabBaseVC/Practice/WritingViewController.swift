//
//  WritingViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 20/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class WritingViewController: UIViewController {

    @IBOutlet weak var cvWriting: UICollectionView!
    var category: PracticeCategoryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         cvWriting.register(UINib(nibName: "PracticeCategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PracticeCategoryCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvWriting.reloadData()
    }

    func setCategoryDataAndUpdate(_ category: PracticeCategoryModel) {
        self.category = category
        if cvWriting != nil {
            cvWriting.reloadData()
        }
    }
}

/*extension WritingViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category?.subCategory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView_writring.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileTableViewCell
        if let subCategory = category?.subCategory {
            cell.lblTitle.text = subCategory[indexPath.row].category
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let practiceDetailVC = PracticeDetailVC.instance() as! PracticeDetailVC
        practiceDetailVC.currentRow = 0
        
        if let subCategory = category?.subCategory {
            practiceDetailVC.categoryId = "\(subCategory[indexPath.row].id!)"
        }
        
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
    }
    
    
}*/

extension WritingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category?.subCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeCategoryCell", for: indexPath) as! PracticeCategoryCell
        
        if let subCategory = category?.subCategory {
            
            cell.lblTitle.text = subCategory[indexPath.row].category
            cell.viewOuter.borderColor = AppColor.clr1
            cell.viewOuter.borderWidth = 1.0
            cell.viewOuter.layer.cornerRadius = 5.0
            cell.lblRequest.clipsToBounds = true
            cell.lblRequest.layer.cornerRadius = 10
            cell.lblRequest.text = TokenRequired
            
            if subCategory[indexPath.row].id == 10 {
                cell.imgCategory.image = #imageLiteral(resourceName: "summarize_written")
            }else if subCategory[indexPath.row].id == 11 {
                cell.imgCategory.image = #imageLiteral(resourceName: "writtten_easy")
                 cell.lblRequest.text = Free
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (cvWriting.frame.width / 2)  // - 0.5
        return CGSize(width:width , height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let practiceDetailVC = PTESortViewController.instance() as! PTESortViewController
        practiceDetailVC.currentRow = 0
        let subCategory = category?.subCategory
        practiceDetailVC.titles = subCategory![indexPath.row].category
        practiceDetailVC.categoryId = "\(subCategory![indexPath.row].id!)"
        staticCategory = category?.category
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
    }
}
