//
//  SpeakingViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 20/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

var staticCategory :String!

class SpeakingViewController: UIViewController {
    
    @IBOutlet weak var cvSpeacking: UICollectionView!
    var category: PracticeCategoryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        cvSpeacking.register(UINib(nibName: "PracticeCategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PracticeCategoryCell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvSpeacking.reloadData()
    }
    
    func setCategoryDataAndUpdate(_ category: PracticeCategoryModel) {
        self.category = category
        if cvSpeacking != nil {
            cvSpeacking.reloadData()
        }
    }
}


extension SpeakingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category?.subCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeCategoryCell", for: indexPath) as! PracticeCategoryCell
        
        if let subCategory = category?.subCategory {
            
            cell.lblTitle.text = subCategory[indexPath.row].category
            cell.viewOuter.borderColor = AppColor.clr1
            cell.viewOuter.borderWidth = 1.0
            cell.viewOuter.layer.cornerRadius = 5.0
            cell.lblRequest.clipsToBounds = true
            cell.lblRequest.layer.cornerRadius = 10
            cell.lblRequest.text = TokenRequired
            if subCategory[indexPath.row].id == 5 {
                cell.imgCategory.image = #imageLiteral(resourceName: "read_aloud")
            }else if subCategory[indexPath.row].id == 6 {
                cell.imgCategory.image = #imageLiteral(resourceName: "repeat_sentence")
            }else if subCategory[indexPath.row].id == 7 {
                cell.imgCategory.image = #imageLiteral(resourceName: "describe_img")
            }else if subCategory[indexPath.row].id == 8 {
                cell.imgCategory.image = #imageLiteral(resourceName: "retell-lecture")
            }else if subCategory[indexPath.row].id == 9 {
                cell.imgCategory.image = #imageLiteral(resourceName: "short_question")
                cell.lblRequest.text = Free
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (cvSpeacking.frame.width / 2)  // - 0.5
        return CGSize(width:width , height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let practiceDetailVC = PTESortViewController.instance() as! PTESortViewController
        let subCategory = category?.subCategory
        practiceDetailVC.titles = subCategory![indexPath.row].category
        practiceDetailVC.categoryId = "\(subCategory![indexPath.row].id!)"
        practiceDetailVC.currentRow = indexPath.row + 1
        staticCategory = category?.category
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
        
        
    }
}


