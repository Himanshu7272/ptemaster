//
//  AnalysisWritingVC.swift
//  PTEMaster
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class AnalysisWritingVC: UIViewController {
    
    @IBOutlet weak var cvWriting: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          cvWriting.register(UINib(nibName: "AnalysisCategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AnalysisCategoryCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvWriting.reloadData()
    }

    func setCategoryDataAndUpdate() {
        // self.category = category
        if cvWriting != nil {
            cvWriting.reloadData()
        }
    }

}


extension AnalysisWritingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
        // return category?.subCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnalysisCategoryCell", for: indexPath) as! AnalysisCategoryCell
        
        //if let subCategory = category?.subCategory {
        
        cell.lblTitle.text = "1"
        cell.viewOuter.backgroundColor = AppColor.clr1
        cell.viewOuter.borderWidth = 1.0
        cell.viewOuter.layer.cornerRadius = 5.0
        
        // }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (cvWriting.frame.width / 4)  - 0.5
        return CGSize(width:width , height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


