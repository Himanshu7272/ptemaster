//
//  AnalysisSpeakingVC.swift
//  PTEMaster
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class AnalysisSpeakingVC: UIViewController {
    
    @IBOutlet weak var cvSpeacking: UICollectionView!
    // var category: PracticeCategoryModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cvSpeacking.register(UINib(nibName: "AnalysisCategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AnalysisCategoryCell")
        setCategoryDataAndUpdate()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvSpeacking.reloadData()
    }
    
   /* func setCategoryDataAndUpdate(_ category: PracticeCategoryModel) {
       // self.category = category
        if cvSpeacking != nil {
            cvSpeacking.reloadData()
        }
    }*/
    
    func setCategoryDataAndUpdate() {
        // self.category = category
        if cvSpeacking != nil {
            cvSpeacking.reloadData()
        }
    }

}


extension AnalysisSpeakingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
       // return category?.subCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnalysisCategoryCell", for: indexPath) as! AnalysisCategoryCell
        
        //if let subCategory = category?.subCategory {
            // let myColor: UIColor =
            cell.lblTitle.text = "1"
            cell.viewOuter.backgroundColor = UIColor.getRandomColor()
            cell.viewOuter.borderWidth = 1.0
            cell.viewOuter.layer.cornerRadius = 5.0
            
       // }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (cvSpeacking.frame.width / 4)  - 0.5
        return CGSize(width:width , height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


extension UIColor {
    
    class func getRandomColor() -> UIColor {
        let colors = [AppColor.clr1 , AppColor.clr2 , AppColor.clr3 , AppColor.clr4]
        let randomNumber = arc4random_uniform(UInt32(colors.count))
        
        return colors[Int(randomNumber)]
    }
    
}
