//
//  PTEMBuyTokensViewController.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTEMBuyTokensViewController: UIViewController {
    
    @IBOutlet weak var lblTokenName: UILabel!
    @IBOutlet weak var lblTokenAmount: UILabel!
    @IBOutlet weak var lblCreditToken: UILabel!
    @IBOutlet weak var lblAmountDiscount: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblDiscunt: UILabel!
    @IBOutlet weak var lblDiscountsPrice: UILabel!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var lblTotalAmount: UILabel!
    var tokenModal:TokenModal?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSetdata(dict: tokenModal!)
        // Do any additional setup after loading the view.
    }
    
    func isSetdata(dict:TokenModal) {
        let price = Double(dict.tk_pkg_price!)!
        let discount = Double(dict.tk_pkg_discount!)!
        let discountAmount =   discount / 100 * price
        let totalAmount = price - discountAmount
        let strAmount = String(format: "%.2f", totalAmount)
        lblTokenName.text = dict.tk_pkg_name
        lblTokenAmount.text = "$" + (dict.tk_pkg_price ?? "0") + "AUD"
        lblAmountDiscount.text = "$" + strAmount + "AUD"
        lblCredits.text = dict.tk_pkg_credits
        lblDiscunt.text = (dict.tk_pkg_discount ?? "0")  + "%"
        lblDiscountsPrice.text = "$" + strAmount + "AUD"
        lblTotalAmount.text =  "$" + strAmount  + "AUD"
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionsBuyToken(_ sender: Any) {
        self.isCallBuyTokenApiCall()
    }
    
    func isCallBuyTokenApiCall() {
        if appDelegate.isNetworkAvailable {
            var postParams =  [String: Any]()
            postParams["user_id"] = AppDataManager.shared.loginData.id
            postParams["token_id"] = tokenModal!.tk_pkg_id!
            postParams["code"] = txtPromoCode.text ??  "0"
            
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl +  RequestPath.token_buynow.rawValue, method: .post, headers: nil) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    if let msg = swiftyJsonResponseDict["msg"].string {
                                        if let url = URL(string: msg) {
                                            UIApplication.shared.open(url)
                                        }
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}

