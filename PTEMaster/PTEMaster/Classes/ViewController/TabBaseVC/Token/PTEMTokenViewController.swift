//
//  PTEMTokenViewController.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTEMTokenViewController: UIViewController {
    @IBOutlet weak var segmentToken: UISegmentedControl!
    @IBOutlet weak var tableToken: UITableView!
    var arrTokenPacks = [TokenModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentToken.selectedSegmentIndex = 1
        self.isCallTokenPacksApiCall(apiActions: RequestPath.token_packs.rawValue)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionSegmentTokens(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.isCallTokenPacksApiCall(apiActions: RequestPath.account_details.rawValue)
            tableToken.reloadData()
        } else if sender.selectedSegmentIndex == 1 {
            self.isCallTokenPacksApiCall(apiActions: RequestPath.token_packs.rawValue)
        }
}
    
    // MARK:-TokenPacks
    func isCallTokenPacksApiCall(apiActions:String) {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "userid" : "\(AppDataManager.shared.loginData.id!)"]
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + apiActions, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(String(describing: dashboradDataDict["user"]))")
                                        if apiActions == RequestPath.token_packs.rawValue {
                                            if let arrTestList = dashboradDataDict["pakage"]?.array {
                                                self.arrTokenPacks.removeAll()
                                                for dict in arrTestList {
                                                    let packs = TokenModal(parameters: dict)
                                                    self.arrTokenPacks.append(packs)
                                                    let indexPath = IndexPath(item: self.arrTokenPacks.count - 1 , section: 0)
                                                    self.tableToken.insertRows(at: [indexPath], with: .automatic)
                                                }
                                            }
                                        } else {
                                            if let arrTestList = dashboradDataDict["pakage"]?.array {
                                                for dict in arrTestList {
                                                    let packs = TokenModal(parameters: dict)
                                                    self.arrTokenPacks.append(packs)
                                                    let indexPath = IndexPath(item: self.arrTokenPacks.count - 1 , section: 0)
                                                    self.tableToken.insertRows(at: [indexPath], with: .automatic)
                                                }
                                            }
                                        }
                                    }
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}

extension PTEMTokenViewController: UITableViewDelegate , UITableViewDataSource , PTEMTokenDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return arrTokenPacks.count
        if segmentToken.selectedSegmentIndex == 0 {
            return 0
        } else if segmentToken.selectedSegmentIndex == 1 {
            return arrTokenPacks.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTEMTokenTableViewCell", for: indexPath) as! PTEMTokenTableViewCell
        cell.delegate = self
        cell.isSetData(dict: arrTokenPacks[indexPath.row])
        return cell
    }
    
    func actionBuyNow(cell: PTEMTokenTableViewCell) {
        guard let indexPath = tableToken.indexPath(for: cell) else {
            return
        }
        
        let dict  = arrTokenPacks[indexPath.row]
        let vc = PTEMBuyTokensViewController.instance() as! PTEMBuyTokensViewController
        vc.tokenModal = dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict  = arrTokenPacks[indexPath.row]
        let vc = PTEMBuyTokensViewController.instance() as! PTEMBuyTokensViewController
        vc.tokenModal = dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
