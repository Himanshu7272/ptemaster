//
//  MockTestDetailVC.swift
//  PTEMaster
//
//  Created by mac on 23/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class MockTestDetailVC: UIViewController {
    
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblSubCategoryName: UILabel!
    @IBOutlet weak var lbTimerCounter: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNextTest: UIButton!
    
    var selectedMockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var currentController: UIViewController?
    var timeLeft: TimeInterval = 60
    var totalTime: TimeInterval = 0
    var countDownTimer: Timer?

    var testId = ""
    var currentQuestionId = ""
    var currentQuestionCount = 1
    var categoryId:String?
    
    var testType : TestType = .mockTest
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getTestApi()
        
    }

    override func viewDidDisappear(_ animated: Bool) {
        invalidateTimer()
    }
    
    //MARK:- set data on view
    func setupScreen() {
        prepareScreenAccordinfToTest()
        lblCategoryName.text = self.selectedMockQestionModel?.categoryName.validate
        lblSubCategoryName.text = (self.selectedMockQestionModel?.subCategoryName.validate)! + " " + "(\((self.selectedMockQestionModel?.questionId.validate)!))"
        startTimer()
    }
    
    func moveToNextQuestion() {
        if let questionId = Int(currentQuestionId) {
            if questionId == 56 {
                Util.showAlertWithCancelCallback("Confirmation Alert!", message: "Would you like to take a quick break ? If not, click continue to resume your test", isWithCancel: true) { (btnTitle) in
                    if btnTitle == "Ok" {
                        self.currentQuestionCount += 1
                        self.getTestApi()
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } else if questionId >= 74 {
                Util.showAlertWithCancelCallback("Confirmation Alert!", message: "Thanks, Your test is completed\n Click continue to see result", isWithCancel: false) { (btnTitle) in
                    // self.navigationController?.popViewController(animated: true)
                    let vc = ReportDetailViewController.instance() as! ReportDetailViewController
                    vc.testId = (self.selectedMockQestionModel?.testId!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                currentQuestionCount += 1
                getTestApi()
            }
        }
    }
    
    //MARK:- button action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        performActionOnSubmit()
    }
    
    @IBAction func btnNextTest(_ sender: Any) {
        performActionOnSubmit()
    }
}


// MARK:- Timer
extension MockTestDetailVC {
    
    func startTimer() {
        invalidateTimer()
        countDownTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            
            self.timeLeft -= 1
            self.updateTimerLabelText()
            self.setNextNumberTitle()
            if self.timeLeft <= 0 {
                timer.invalidate()
                self.invalidateTimer()
            }
        })
    }
    
    
    func invalidateTimer() {
        self.countDownTimer?.invalidate()
        self.countDownTimer = nil
        self.timeLeft = (self.selectedMockQestionModel?.totalTimeRemaining) ?? 0.0
       // self.totalTime = self.timeLeft
        
        self.totalTime = (self.selectedMockQestionModel?.totalTimeRemaining) ?? 0.0
    }
    
    func updateTimerLabelText() {
        if self.timeLeft > 0 {
            let minutes = self.timeLeft / 60
            let seconds = self.timeLeft.truncatingRemainder(dividingBy: 60)
            let text = String(format: "%02i:%02i", Int(minutes), Int(seconds))
            lbTimerCounter.text = text
            updateScreenTimer()
        } else {
            lbTimerCounter.text = "00:00"
        }
    }
    

    func setNextNumberTitle() {
    
       var current_ques_count = 0
        switch self.selectedMockQestionModel?.totalQuestion {
            
        case 36:
            current_ques_count = self.selectedMockQestionModel?.questionNumber ?? 0
        case 4:
            current_ques_count = (self.selectedMockQestionModel?.questionNumber ?? 0) - 36
        case 16:
            current_ques_count = (self.selectedMockQestionModel?.questionNumber ?? 0) - (36 + 4)
        case 18:
            current_ques_count = (self.selectedMockQestionModel?.questionNumber ?? 0) - (36 + 4 + 16)
    
        default:
            current_ques_count = self.selectedMockQestionModel?.questionNumber ?? 0
        }
        
        let str = "\(current_ques_count)" + "/" + "\(self.selectedMockQestionModel?.totalQuestion ?? 0)"
        btnNextTest.setTitle("\(str)   > " , for: .normal)
        
    }
    
    func updateScreenTimer() {
        //Speaking
        
         if let vc = self.currentController as? ReadAloudVC {
            vc.updateTimer()
         }else if let vc = self.currentController as? ReapeatSentenceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? DescribeImageVC {
            vc.updateTimer() // correct 
         }else if let vc = self.currentController as? ShortAnswereVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? RetellAnswereVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }
         
            // writing ...
         else if let vc = self.currentController as? SummarizeWrittenTextVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? WritingEasyVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }
         
            
         else if let vc = self.currentController as? SummerySpokenVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? ListingMultipleChoiceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? HighlightCorrectSummeryVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? ListningSingleChoiceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? SelectMissingWordVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? HighlightIncorrectWordsVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? ListningFillInBlanksVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
         }else if let vc = self.currentController as? WriteDictationVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }
        
            // reading ...
         else if let vc = self.currentController as? SingleChoiceVC {
            vc.updateTimer()
         }else if let vc = self.currentController as? MultipleChoiceVC {
            vc.updateTimer()
         }else if let vc = self.currentController as? ReOrderVC {
            vc.updateTimer()
         }else if let vc = self.currentController as? ReadWriteBlanksVC {
            vc.updateTimer()
         }else if let vc = self.currentController as? FillInTheBlanksVC {
            vc.updateTimer()
        }
    }

}

extension MockTestDetailVC {
    
    func getTestApi() {
        if appDelegate.isNetworkAvailable {
            
            // currentQuestionId             // \(currentQuestionId)

            // 18 row descibe image
            // 48  "Reading: Fill in the blanks
            // 51 Reading / writing : Fill in the blanks
            //\(currentQuestionId)
            
            let postParams: [String: Any] = ["testid": "\(testId)", "rowperpage": "1", "row": "\(currentQuestionId)", "user_id": "\(AppDataManager.shared.loginData.id!)"]
            
            // let postParams: [String: Any] = ["testid": "29", "rowperpage": "1", "row": "536", "user_id": "\(AppDataManager.shared.loginData.id!)"]

            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.getTestApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            // self.setupScreen()
                            //  Loader.hideLoader()
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                                print("swiftyJsonResponseDict >>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary, let mockqestion = dashboradDataDict["mockqestion"]?.array {
                                    
                                    self.arrOptionModel.removeAll()
                                    if let optionList = dashboradDataDict["option"]?.array, optionList.count > 0 {
                                        for dict in optionList {
                                            let optionModel = OptionsModel(json: dict)
                                            self.arrOptionModel.append(optionModel)
                                        }
                                        print(self.arrOptionModel.count)
                                    }
                                    
                                    if let first = mockqestion.first {
                                        print(first)
                                        self.selectedMockQestionModel = MockQestionModel(json: first)
                                        self.setupScreen()
                                    }
                                    
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
    
    //MARK:- answere api for test
    func callAnswerAPIWithParameters(_ param: [String: Any] , ansApi : String) {
        
        if appDelegate.isNetworkAvailable {
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
                    
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + ansApi , method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                print("swiftyJsonResponseDict >>>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                    let nextId = dashboradDataDict["next"]?.stringValue
                                    self.currentQuestionId = nextId.validate
                                    self.moveToNextQuestion()
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}


//MARK:- Child Controllers
extension MockTestDetailVC {
    
    func prepareScreenAccordinfToTest() {
        if let mockTestModel = selectedMockQestionModel {
            
            // speaking .....
            if mockTestModel.categoryType == CategoryType.readAloud {
                let vc = ReadAloudVC.instance(.question) as! ReadAloudVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.repeatSentence {
                let vc = ReapeatSentenceVC.instance(.question) as! ReapeatSentenceVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.describeImg {
                let vc = DescribeImageVC.instance(.question) as! DescribeImageVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.reTellLecture {
                let vc = RetellAnswereVC.instance(.question) as! RetellAnswereVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.ansShortQuestn {
                let vc = ShortAnswereVC.instance(.question) as! ShortAnswereVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }
            
                // writing ...
            else if mockTestModel.categoryType == CategoryType.summarizeWrittenText {
                let vc = SummarizeWrittenTextVC.instance(.question) as! SummarizeWrittenTextVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.writingEasy {
                let vc = WritingEasyVC.instance(.question) as! WritingEasyVC
                vc.mockQestionModel = self.selectedMockQestionModel
                updateContentViewChildController(vc)
            }
            
                // reading ....
            else if mockTestModel.categoryType == CategoryType.singleChoice {
                let vc = SingleChoiceVC.instance(.question) as! SingleChoiceVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.multipleAnswere {
                let vc = MultipleChoiceVC.instance(.question) as! MultipleChoiceVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.reOrderParagraph {
                let vc = ReOrderVC.instance(.question) as! ReOrderVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.readFillInBlanks {
                let vc = FillInTheBlanksVC.instance(.question) as! FillInTheBlanksVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.readWriteBlanks {
                let vc = ReadWriteBlanksVC.instance(.question) as! ReadWriteBlanksVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }
            
                // listening
            else if mockTestModel.categoryType == CategoryType.listSummarySpokenText {
                let vc = SummerySpokenVC.instance(.question) as! SummerySpokenVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listMultipleChoiceMultipleAns {
                let vc = ListingMultipleChoiceVC.instance(.question) as! ListingMultipleChoiceVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listFillInTheBlanks {
                let vc = ListningFillInBlanksVC.instance(.question) as! ListningFillInBlanksVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            } else if mockTestModel.categoryType == CategoryType.listHighlightCorrectSummery {
                let vc = HighlightCorrectSummeryVC.instance(.question) as! HighlightCorrectSummeryVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listMultipleChoiceSingleAns {
                let vc = ListningSingleChoiceVC.instance(.question) as! ListningSingleChoiceVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listSelectMissingWord {
                let vc = SelectMissingWordVC.instance(.question) as! SelectMissingWordVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listHighlightInCorrectWord {
                let vc = HighlightIncorrectWordsVC.instance(.question) as! HighlightIncorrectWordsVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel.categoryType == CategoryType.listWriteFromDictation {
                let vc = WriteDictationVC.instance(.question) as! WriteDictationVC
                vc.mockQestionModel = self.selectedMockQestionModel
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }
        }
    }
    
    //MARK:- ans api on submit button
    func performActionOnSubmit() {
        
         // speaking ....
        if let vc = currentController as? ReapeatSentenceVC {
            vc.stopRecordingAndCallAnsAPI()
        }else  if let vc = currentController as? ReadAloudVC {
            vc.stopRecordingAndCallAnsAPI()
        }else  if let vc = currentController as? DescribeImageVC {
            vc.stopRecordingAndCallAnsAPI()
        }else if let vc = currentController as? ShortAnswereVC {
            vc.stopRecordingAndCallAnsAPI()
        }else  if let vc = currentController as? RetellAnswereVC {
            vc.stopRecordingAndCallAnsAPI()
        }
        
         // listening ....
        else  if let vc = currentController as? WriteDictationVC {
            vc.callAnsAPI()
        }else  if let vc = currentController as? SummerySpokenVC {
            vc.callAnsAPI()
        }else  if let vc = currentController as? ListingMultipleChoiceVC {
            vc.callAnsAPI()
        }else  if let vc = currentController as? ListningSingleChoiceVC {
             vc.callAnsAPI()
        }else  if let vc = currentController as? HighlightCorrectSummeryVC {
             vc.callAnsAPI()
        }else  if let vc = currentController as? SelectMissingWordVC {
             vc.callAnsAPI()
        }else if let vc = currentController as? ListningFillInBlanksVC {
            vc.callAnsAPI()
        }else if let vc = currentController as? HighlightIncorrectWordsVC {
            vc.callAnsAPI()
        }

        // writing ....
        else  if let vc = currentController as? WritingEasyVC {
             vc.callAnsAPI()
        }else  if let vc = currentController as? SummarizeWrittenTextVC {
             vc.callAnsAPI()
        }
        
        // reading ...
        else  if let vc = currentController as? SingleChoiceVC {
            vc.callAnsAPI()
        } else  if let vc = currentController as? MultipleChoiceVC {
            vc.callAnsAPI()
        } else  if let vc = currentController as? ReOrderVC {
            vc.callAnsAPI()
        } else  if let vc = currentController as? FillInTheBlanksVC {
            vc.callAnsAPI() // gragaabble collection view here
        } else  if let vc = currentController as? ReadWriteBlanksVC {
            vc.callAnsAPI()
        }
    }
    
    func updateContentViewChildController(_ vc: UIViewController) {
        if let preVC = currentController {
            preVC.willMove(toParent: nil)
            preVC.view.removeFromSuperview()
            preVC.removeFromParent()
            currentController = nil
        }
        currentController = vc
        vc.willMove(toParent: vc)
        vc.view.frame = self.viewContainer.bounds;
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin];
        self.viewContainer.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
    }

}


enum TestType {
    
    case practice
    case mockTest
}
