//
//  MockTestDetailViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 14/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class MockTestDetailViewController: UIViewController
{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTotalQuestion: UILabel!
    @IBOutlet weak var lblTimeDuration: UILabel!
    @IBOutlet weak var lblDescryptions: UILabel!
    @IBOutlet weak var txfpromoCode: UITextField!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblDiscountPercent: UILabel!
    @IBOutlet weak var lblCouponDescription: UILabel!
    @IBOutlet weak var lblCouponName: UILabel!
    @IBOutlet weak var viewPromoCodeDetail: UIView!

    @IBOutlet weak var btnCancelPromoCode: UIButton!
    
    @IBOutlet weak var consViewPromoCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var consBtnCancelHeight: NSLayoutConstraint!
    
    var oldPromoCodePrice = ""
    var newPrice = ""
    var arrMockDetail = [MockDetailCategoryModel]()
    var arrMockDetailCateoryTitle = [TestDetailModel]()
    var testId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrMockDetail = [MockDetailCategoryModel(title: "Speaking" , img: "speaking_icon"),
                         MockDetailCategoryModel(title: "Writing" , img: "writing_icon"),
                         MockDetailCategoryModel(title: "Reading" , img: "reading_icon"),
                         MockDetailCategoryModel(title: "listening" , img: "listening_icon")]
        TestDetailsApi()
        consViewPromoCodeHeight.constant = 0
        consBtnCancelHeight.constant = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Check Promo Code
    func isValidPromoCode() -> Bool {
        if !Util.isValidString(txfpromoCode.text!) {
            Util.showAlertWithMessage("Enter valid promo code", title: Key_Alert)
            return false
        }
        return true
    }
    
    
    func setData(_ info: TestDetailModel ) {
        lblTotalQuestion.text = info.totalQuestionNumber.validate
        lblTimeDuration.text = info.time.validate
        lblPrice.text = "$" + info.price.validate + " AUD"
        oldPromoCodePrice = info.price.validate
        lblDescryptions.text = info.desciption.validate
    }
    
    func setDataOnPromoCode(_ info: CoupleInfoModel) {
       
        lblCouponDescription.text = info.title.validate
        lblCouponName.text = info.code.validate
        lblDiscountPercent.text = "Discount : \(info.discount.validate)  %"
        btnCancelPromoCode.setTitle("X", for: .normal)
        
      
       // let perCent = 100*Double(oldPromoCodePrice)!/Double(info.discount.validate)!
        
        let newPromoCodePrice = Double(oldPromoCodePrice)! - (100*Double(oldPromoCodePrice)!/Double(info.discount.validate)!)
        print(newPromoCodePrice)
        lblPrice.text = ""
        lblPrice.text = "$" + "\(newPromoCodePrice)" + " AUD"
    }
    
    
    // MARK:- Action Method
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubscribe_Action(_ sender: Any) {
        //viewPromoCodeDetail.isHidden = true
        SubscribeTestApi()
    }
    
    @IBAction func btnApplyPromocode_Action(_ sender: Any) {
        if isValidPromoCode() {
            PromoCodesApi()
        }
    }
    
    @IBAction func btnCancelCode(_ sender: Any) {
        
        consViewPromoCodeHeight.constant = 0
        consBtnCancelHeight.constant = 0
        btnCancelPromoCode.setTitle("", for: .normal)
    }
    
    
    //MARK:- Test Detail Api
    func TestDetailsApi() {
        if appDelegate.isNetworkAvailable {
            
            
            let postParams: [String: Any] = [ "test_id" : testId, "user_id": "\(AppDataManager.shared.loginData.id!)"]
            
           // let postParams: [String: Any] = [ "test_id" : "29", "user_id": "\(AppDataManager.shared.loginData.id!)"]

            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.MockTestDetailApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let responceDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(responceDict)")
                                        
                                        if let dict = responceDict["test"]?.dictionary {
                                            
                                            //let providerModel = ProviderListModel(providerInfo)
                                             let testDetailInfo = TestDetailModel(dict)
                                            DispatchQueue.main.async {
                                                self.setData(testDetailInfo)
                                            }
                                            
                                        }
                                        
                                        if let arrCategoryList = responceDict["category"]?.array {
                                            for dict in arrCategoryList {
                                                let detailInfoModel = TestDetailModel(json: dict)
                                                self.arrMockDetailCateoryTitle.append(detailInfoModel)
                                            }
                                            self.collectionView.reloadDataInMain()
                                        }
                                        
                                    }
                                   
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }

        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
    
    
    func PromoCodesApi() {
        if appDelegate.isNetworkAvailable {
            
            let postParams: [String: Any] = ["test_id": testId , "promo": "\(txfpromoCode.text!)","user_id": "\(AppDataManager.shared.loginData.id!)"]
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.TestPromoCodeCkeck.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                              appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let dataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(dataDict)")
                                        
                                         if let coupleDict = dataDict["coupon"]?.dictionary {
                                            
                                            let couponDetail = CoupleInfoModel(coupleDict)
                                            self.consViewPromoCodeHeight.constant = 57
                                            self.consBtnCancelHeight.constant = 30
                                            DispatchQueue.main.async {
                                                self.setDataOnPromoCode(couponDetail)
                                            }
                                         }
                                        
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
    
    
    func SubscribeTestApi() {
        if appDelegate.isNetworkAvailable {
            
            let postParams: [String: Any] = ["test_id": testId, "code": "\(txfpromoCode.text!)" , "user_id": "\(AppDataManager.shared.loginData.id!)"]
            print("Post Parameter is:\(postParams)")
            
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.subscribeTest.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                
                                    let vc = PaymentVC.instance() as! PaymentVC
                                    vc.strFrom = swiftyJsonResponseDict ["msg"].stringValue
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}


//MARK:-   Collection View Delegate
extension MockTestDetailViewController :  UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMockDetailCateoryTitle.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ProfileCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCollectionViewCell
        
        cell.lblNumberTest.text = arrMockDetailCateoryTitle[indexPath.row].totalQuestion.validate + " Question"
        
        var str = arrMockDetailCateoryTitle[indexPath.row].category.validate
        let wordToRemove = "Section"

        if let range = str.range(of: wordToRemove) {
            str.removeSubrange(range)
        }
        cell.lblTaskName.text = str
        
        if indexPath.row == 0 {
            
            cell.img_Icon.image = #imageLiteral(resourceName: "speaking_icon")
            
        }else if indexPath.row == 1 {
            
            cell.img_Icon.image = #imageLiteral(resourceName: "writing_icon")
        }else if indexPath.row == 2 {
            
            cell.img_Icon.image = #imageLiteral(resourceName: "reading_icon")
        }else if indexPath.row == 3 {
            
            cell.img_Icon.image = #imageLiteral(resourceName: "listening_icon")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.view.frame.width/3-20,height:95)
    }
}
