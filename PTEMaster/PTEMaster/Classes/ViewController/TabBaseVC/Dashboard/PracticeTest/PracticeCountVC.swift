//
//  PracticeCountVC.swift
//  PTEMaster
//
//  Created by mac on 16/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class PracticeCountVC: UIViewController {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblStr: UILabel!
    @IBOutlet weak var collectionCount: UICollectionView!
    
    var selectedIndex: Int = 0
    var arrQuestionCount = [Int]()
    var chooseQuestionNumber : ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // print(arrQuestionCount)
        
          // time left show krana h yha ...........
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension PracticeCountVC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrQuestionCount.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionCount.dequeueReusableCell(withReuseIdentifier: "PracticeCountCell", for: indexPath) as! PracticeCountCell
        
        cell.lblStr.layer.cornerRadius = cell.lblStr.frame.size.height * 0.5
        cell.lblStr.layer.masksToBounds = true
        cell.lblStr.layer.borderWidth = 2.0
        
        cell.lblStr.text = "\(arrQuestionCount[indexPath.row])"
        
        if indexPath.row == selectedIndex {
            cell.lblStr.backgroundColor = UIColor.yellow
            cell.lblStr.layer.borderColor = UIColor.blue.cgColor
        }else {
            cell.lblStr.backgroundColor = UIColor.clear
            cell.lblStr.layer.borderColor = UIColor.black.cgColor
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionCount.reloadDataInMain()
        
        if let block = chooseQuestionNumber {
            block(indexPath.row)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeWidth = self.collectionCount.frame.width / 5 - 10
        return CGSize(width: sizeWidth, height: sizeWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
