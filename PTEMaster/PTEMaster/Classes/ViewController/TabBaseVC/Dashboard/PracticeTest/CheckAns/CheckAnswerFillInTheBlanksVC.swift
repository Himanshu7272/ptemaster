//
//  CheckAnswerFillInTheBlanksVC.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 22/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class CheckAnswerFillInTheBlanksVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var answers = [CheckAnswerFTBModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    static func open(in viewController: UIViewController, answers: [CheckAnswerFTBModel]) {
        let vc = CheckAnswerFillInTheBlanksVC.instance() as! CheckAnswerFillInTheBlanksVC
        vc.answers = answers
        viewController.present(vc, animated: true, completion: nil)
    }

}


extension CheckAnswerFillInTheBlanksVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckAnswerFillInTheBlanksCell") as! CheckAnswerFillInTheBlanksCell
        cell.setupData(answers[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}


struct CheckAnswerFTBModel {
    var correctAnswer: String
    var userAnswer: String
}
