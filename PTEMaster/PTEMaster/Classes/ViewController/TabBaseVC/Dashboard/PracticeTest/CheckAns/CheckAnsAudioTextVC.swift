//
//  CheckAnsAudioTextVC.swift
//  PTEMaster
//
//  Created by mac on 08/03/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

class CheckAnsAudioTextVC: UIViewController {

    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblStr: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    
    var ansType = ""
    var strAnsText = ""
    var strAnsAudio = ""
    var arrAns = [OptionsModel]()
    private let audioEngine = AVAudioEngine()
    
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    private var playerItemContext = 0
    var screenStatus = ScreenStatus.preparingToPlay
    
    enum CheckAnsType: String {
        case both = "both"
    }
    
    enum ScreenStatus {
        case preparingToPlay
        case waiting
        case playingAudio
        case recognizingText
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if CheckAnsType.both.rawValue == "both" {
             //self.prepareAudio(from: BaseUrlTestQuestion + "0" + "/" + "5b9388c48bd8b.mp3")
            self.prepareAudio(from: BaseUrlTestQuestion + "0" + "/" + "\(strAnsAudio)")
        }
        
        setUpViewLayout()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
    }
    
    func setUpViewLayout() {
    
        if CheckAnsType.both.rawValue == "both" {
             lblAnswer.text = strAnsText
             screenStatus = .playingAudio
             playAudio()
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        stopPlaying()
        self.dismiss(animated: true, completion: nil)
    }

    

}

extension CheckAnsAudioTextVC: AVAudioPlayerDelegate {
    
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            appDelegate.showHUD("Prepare audio", onView: self.view)
            avPlayerItem = AVPlayerItem(url: url)
            avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
            audioPlayer = AVPlayer(playerItem: avPlayerItem)
            audioPlayer?.pause()
        }
    }
    
    // MARK:- Pre-audio
    func playAudio() {
        if self.screenStatus == .playingAudio {
            audioPlayer?.play()
            let interval = CMTime(value: 1, timescale: 2)
            audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                let seconds = CMTimeGetSeconds(progressTime)
                if let duration = self.audioPlayer?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let progress = Float(seconds / durationSeconds)
                    self.audioProgress.progress = progress
                    if progress >= 1 {
                        self.updateScreenWhenAudioStops()
                    }
                }
            })
        }
    }
    
    
    // MARK:- Pre-audio
    func stopPlaying() {
        audioPlayer?.pause()
        audioPlayer = nil
        avPlayerItem = nil
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Pre-audio
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            screenStatus = .waiting
            appDelegate.hideHUD(self.view)
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
}
