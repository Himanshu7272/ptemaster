//
//  DashboardViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 19/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController
{
    
    var arrTestList = [DashBordTestListModel]()
    var arrCategory = [MockDetailCategoryModel]()
var count : Int = 0
    @IBOutlet weak var tableView_MockList: UITableView!
    @IBOutlet weak var collectionView_test: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrCategory = [MockDetailCategoryModel(title: "Attempt Test" , img: "attempt_icon"),
                         MockDetailCategoryModel(title: "Completed Test" , img: "completed_test"),
                         MockDetailCategoryModel(title: "Mock Test" , img: "mocktest_icon")]
        
        tableView_MockList.estimatedRowHeight = 100
        tableView_MockList.rowHeight = UITableView.automaticDimension
        
        //self.dashboardnApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         count  = 0
         self.dashboardnApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Method
    @IBAction func btnViewAllMockTest_Action(_ sender: Any) {
        let vc = MockTestViewController.instance() as! MockTestViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK:- Dashborad Api
    func dashboardnApi() {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "user_id" : "\(AppDataManager.shared.loginData.id!)"]
            
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.DashboradApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(dashboradDataDict)")
                                        
                                        if let arrTestList = dashboradDataDict["testlist"]?.array {
                                            
                                            for dict in arrTestList {
                                                let dashboradlistModel = DashBordTestListModel(json: dict)
                                                self.arrTestList.append(dashboradlistModel)
                                            }
                                            self.tableView_MockList.reloadDataInMain()
                                        }
                                       
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}


//MARK:- Collection View Delegate
extension DashboardViewController :  UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategory.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:DashboardCollectionCell = collectionView_test.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath) as! DashboardCollectionCell
        
        cell.lblTitle.text = arrCategory[indexPath.row].title.validate
       // let imagesUrl = URL(string: ImageBaseUrl.questionImageUrl + arrCategory[indexPath.row].img.validate)
       //cell.img_icon.kf.setImage(with: imagesUrl)
        cell.img_icon.image = UIImage(named: arrCategory[indexPath.row].img.validate)
        
        if indexPath.row == 1 {
            cell.view_bg.backgroundColor =  AppColor.greenClr
        }else if indexPath.row == 2 {
            cell.view_bg.backgroundColor   = AppColor.redColor
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if indexPath.row == 0
        {
            let vc = AttemptTestListViewController.instance() as! AttemptTestListViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
        
            let vc =  CompletedTestListViewController.instance() as! CompletedTestListViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            let vc = MockTestViewController.instance() as! MockTestViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.view.frame.width/3-12,height:100)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      cell.alpha = 0
      cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
      UIView.animate(withDuration: 0.25) {
        cell.alpha = 1
        cell.transform = .identity
      }
    }
}


extension DashboardViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
//        if arrTestList.count >= 5 {
//            return 4
//        }
        return arrTestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView_MockList.dequeueReusableCell(withIdentifier: "DashBoardtestListCell") as! DashBoardtestListCell

        cell.setData(info: arrTestList[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrTestList[indexPath.row].status.validate == "0" { // buy now
            //\(arrTestList[indexPath.row].id!)
            let mockDetailVC = MockTestDetailViewController.instance() as! MockTestDetailViewController
            
            // first test id ..... is  "\(arrTestList[indexPath.row].question_number!)"
            // next id milegi ans api me "next" key me
            
            mockDetailVC.testId = "\(arrTestList[indexPath.row].question_number!)"
            //
            self.navigationController?.pushViewController(mockDetailVC, animated: true)
        
        } else if arrTestList[indexPath.row].status.validate == "1" {   //subscribe
            
        } else if arrTestList[indexPath.row].status.validate == "2" {   //start test
            
            Util.showAlertWithCallback("Confirmation Alert !", message: "\(arrTestList[indexPath.row].name!)?", isWithCancel: true) {
                
                print("ok")
                
                let mockDetailVC = MockTestDetailVC.instance() as! MockTestDetailVC
                mockDetailVC.testId = "\(self.arrTestList[indexPath.row].id!)"
                
                // 56 - summery spoken
                // 58 - choose multiple
                // 60 - fill in blanks
                // 62 - Highlight correct summary
                // 64 - Multiple-choice, choose single answer
                // 66 - Select missing word
                // 68 -  Highlight incorrect words
                //  70 -  Write from dictation
                // 50 - reading - fill in the blanks
                
                // 51 reading / writng blanks
                
                // 5 - read aloud
                // 25 - Re-tell Lecture
                
                // 42 - Multiple-choice, choose multiple answer
                    
                mockDetailVC.currentQuestionId = self.arrTestList[indexPath.row].question_number.validate
                    //self.arrTestList[indexPath.row].question_number.validate
                self.navigationController?.pushViewController(mockDetailVC, animated: true)
                
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if count  != arrTestList.count{
             count += 1
        let animation = AnimationFactory.makeSlideIn(duration: 0.2, delayFactor: 0.02)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
    
}
