//
//  CompletedTestListViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 15/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class CompletedTestListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrHistoryTest = [HistoryListModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        historyListApi()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func historyListApi() {
        if appDelegate.isNetworkAvailable {
            arrHistoryTest.removeAll()
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            // "\(AppDataManager.shared.loginData.id!)"
            // let param : [String: Any] = ["user_id" : "69"]
            
            let param : [String: Any] = ["user_id" : "\(AppDataManager.shared.loginData.id!)"]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.testHistoryList.rawValue , method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                                print("swiftyJsonResponseDict >>>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                    
                                    print("dashboradDataDict  >>>>>> \(dashboradDataDict)")
                                    
                                    if let arrTest = dashboradDataDict["test"]?.array {
                                        
                                        print(arrTest.count)
                                        
                                        
                                        if arrTest.count == 0 {
                                            self.tableView.displayBackgroundText(text: "No Completed Test Yet!")
                                        }else {
                                            self.tableView.backgroundView = nil
                                            for dict in arrTest {
                                                let historyListModel = HistoryListModel(json: dict)
                                                
                                                if historyListModel.completedStatut == "1" {
                                                    self.arrHistoryTest.append(historyListModel)
                                                }
                                            }
                                            self.tableView.reloadDataInMain()
                                        }
                            
                                    }
                                    
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
    
    //MARK:- taaped button
    func reportHistoryTapped(_ indexPath: IndexPath) {
        
        print("go to history ")
        print(indexPath.row)
        let vc = ReportDetailViewController.instance() as! ReportDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func anylasisTapped(_ indexPath: IndexPath) {
        
        print("go to analysis ")
    }
    
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension CompletedTestListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistoryTest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "HistoryListCell") as! HistoryListCell
        
        cell.setData(info: arrHistoryTest[indexPath.row], reportClickTapped: {
            
            
            self.reportHistoryTapped(indexPath)
            
        }, anylysisClickTapped: {
            
            print(indexPath.row)
            self.anylasisTapped(indexPath)
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}

