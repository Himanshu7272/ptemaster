//
//  MockTestViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 13/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class MockTestViewController: UIViewController {
    
    @IBOutlet weak var tableViewTestList: UITableView!
    
    var categoryName: [String] = ["ATTEMPT TEST", "COMPLETED", "TOTAL TEST"]
    var arrTestList = [DashBordTestListModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

         // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {
         self.dashboardnApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- Dashborad Api
    func dashboardnApi() {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "user_id" : "\(AppDataManager.shared.loginData.id!)"]
            
               appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.DashboradApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(dashboradDataDict)")
                                        
                                        if let arrTestList = dashboradDataDict["testlist"]?.array {
                                            
                                            for dict in arrTestList {
                                                let dashboradlistModel = DashBordTestListModel(json: dict)
                                                self.arrTestList.append(dashboradlistModel)
                                            }
                                            self.tableViewTestList.reloadDataInMain()
                                        }
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
    
}


extension MockTestViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableViewTestList.dequeueReusableCell(withIdentifier: "DashBoardtestListCell") as! DashBoardtestListCell
        cell.setData(info: arrTestList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
        if arrTestList[indexPath.row].status.validate == "0" { // buy now
            
            let mockDetailVC = MockTestDetailViewController.instance() as! MockTestDetailViewController
            mockDetailVC.testId =  "\(arrTestList[indexPath.row].id!)"
            self.navigationController?.pushViewController(mockDetailVC, animated: true)
            
        } else if arrTestList[indexPath.row].status.validate == "1" {   //subscribe
            
        } else if arrTestList[indexPath.row].status.validate == "2" {   //start test
            
            
            Util.showAlertWithCallback("Confirmation Alert !", message: "\(arrTestList[indexPath.row].name!)?", isWithCancel: true) {
                print("ok")
                
                let mockDetailVC = MockTestDetailVC.instance() as! MockTestDetailVC
                mockDetailVC.testId = "\(self.arrTestList[indexPath.row].id!)"
                // mockDetailVC.testId = "\(self.arrTestList[indexPath.row].question_number!)"
                self.navigationController?.pushViewController(mockDetailVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
