//
//  LoginViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 11/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txfEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txfPassword: JVFloatLabeledTextField!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialView()
        btnLogin.transform = CGAffineTransform(rotationAngle: -89.55)
        btnRegister.transform = CGAffineTransform(rotationAngle: -89.55)
     
         // txfEmail.text = "anshul0@mailinator.com"
         // "deva@mailinator.com"
         //  txfPassword.text = "123456"
        
         txfEmail.text = "ctnidhi10@gmail.com"
         txfPassword.text = "123456"
        
    }
    
    func setInitialView() {
        
        if let loginDict = UserDefaults.standard.dictionary(forKey: loginDetail)  {
            AppDataManager.shared.loginData = LoginModel(loginDetail: loginDict)
            let DashboardVC = CustomTabBarController.instance() as! CustomTabBarController
            self.navigationController?.setViewControllers([DashboardVC], animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Method
    func isValidLogin() -> Bool {
        if !Util.isValidString(txfEmail.text!) {
            Util.showAlertWithMessage("Enter e-mail", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txfEmail.text!) {
            Util.showAlertWithMessage("Enter Valid e-mail", title: Key_Alert)
            return false
        }  else if !Util.isValidString(txfPassword.text!) {
            Util.showAlertWithMessage("Enter Valid Password", title: Key_Alert)
            return false
        }
        return true
    }

    //MARK: IBAction Method
    @IBAction func doclickonForgotPassword(_ sender: Any) {
        let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    @IBAction func doclickonLogin(_ sender: Any) {
        if isValidLogin() {
            LoginApi()
        }
    }
    
    @IBAction func doclickonRegister(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    
    // MARK:- Login Api
    func LoginApi() {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: AnyObject] =
                [ "email" : txfEmail.text as AnyObject,
                    "password" : txfPassword.text as AnyObject]
            
            appDelegate.showHUD(appDelegate.strLoader, onView: view)
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.LoginApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD(self.view)
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                
                                    if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(loginDict)")
                                        
                                        let loginDataModel = LoginModel(loginDict)
                                        AppDataManager.shared.loginData = loginDataModel
                                        UserDefaults.standard.set(loginDataModel.getDictionary(), forKey: loginDetail)
                                        let DashboardVC = CustomTabBarController.instance()
                                        self.navigationController?.pushViewController(DashboardVC, animated: true)
                                      }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD(self.view)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage("appDelegate.strInternetError", title: "")
        }
    }
}
