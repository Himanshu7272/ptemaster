//
//  FillInTheBlanksVC.swift
//  PTEMaster
//
//  Created by mac on 04/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class FillInTheBlanksVC: UIViewController {

    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var collectionViewFillInBlanks: UICollectionView!
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    @IBOutlet weak var constCollectionViewOptionsHeight: NSLayoutConstraint!

    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?

    var arrOptionModel = [OptionsModel]()
    var timeOnScreen: TimeInterval = 0
    var arrFillInBlanks = [FillInBlanksModel]()
    var selectedSequence = 1
    var showCorrectAns = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         setupData()
    }
    
    
    func setupData() {
        let new = arrOptionModel.shuffled()
        arrOptionModel = new
        
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            setupDescriptionCollectionView(mockQestionModel.description.validate.removeHtmlFromString())
        } else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
        //    print(mockQestionModel.description)
            setupDescriptionCollectionView(mockQestionModel.description.validate.removeHtmlFromString())
        }
        DispatchQueue.main.async {
            self.updateOptionsCollectionViewHeight()
        }
       // print(arrOptionModel.count)
    }
    
    func setupDescriptionCollectionView(_ description: String) {
       // print("description===\(description)")
        let strSeprated = description.components(separatedBy: " ")
        //print("strSeprated===\(strSeprated)")
        
        for i in 0 ..< strSeprated.count {
            if strSeprated[i].trimWhitespaces.count > 0 {
              
                var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "")
                
                for j in 0 ..< arrOptionModel.count {
                    let sequence = "{\(j+1)}"
                 //   print(sequence)
                 //   print(strSeprated[i])
                    if strSeprated[i].range(of: sequence) != nil {
                       // print(",match with --\(strSeprated[i])-\(sequence)-")
                        fillInTheBlanks.word = ""
                        fillInTheBlanks.isEditable = true
                        fillInTheBlanks.sequence = j+1
                        fillInTheBlanks.placeholder = ""
                        fillInTheBlanks.correctAns = arrOptionModel[j].option.validate /*strSeprated[i]*/
                        fillInTheBlanks.id = ""
                    }
                }
                arrFillInBlanks.append(fillInTheBlanks)
            }
        }
         // print(arrFillInBlanks)
        collectionViewFillInBlanks.reloadDataInMain()
    }
    
    func updateOptionsCollectionViewHeight() {
        var numberOfRows = 1
        if arrOptionModel.count > 0 {
            numberOfRows = arrOptionModel.count / 3
            if arrOptionModel.count % 3 != 0 {
                numberOfRows += 1
            }
        }
        constCollectionViewOptionsHeight.constant = CGFloat(40 * numberOfRows)
    }
    
    func updateTimer() {
        timeOnScreen += 1
    }
    
 
    /*func checkAns2() { // previous function
        var answers = [CheckAnswerFTBModel]()
        for i in 0 ..< arrFillInBlanks.count {
            if let sequence = arrFillInBlanks[i].sequence {
                var answer = CheckAnswerFTBModel(correctAnswer: "", userAnswer: arrFillInBlanks[i].word)
                let filter = arrOptionModel.filter({ $0.correct == "\(sequence)" })
                if let first = filter.first {
                    answer.correctAnswer = first.option.validate
                }
                if answer.userAnswer.count == 0 {
                    answer.userAnswer = "\(sequence)"
                }
                answers.append(answer)
            }
        }
        CheckAnswerFillInTheBlanksVC.open(in: self, answers: answers)
    }*/
    
       func checkAns() {
        var answers = [CheckAnswerFTBModel]()
        for i in 0 ..< arrFillInBlanks.count {
            if arrFillInBlanks[i].sequence != nil {
                var answer = CheckAnswerFTBModel(correctAnswer: "", userAnswer: arrFillInBlanks[i].word)
                answer.correctAnswer = arrFillInBlanks[i].correctAns
                if answer.userAnswer.count == 0 || answer.userAnswer.trimWhitespaces == "_\(arrFillInBlanks[i].sequence!)_" {
                    answer.userAnswer = "-"
                }
                answers.append(answer)
            }
        }
        CheckAnswerFillInTheBlanksVC.open(in: self, answers: answers)
    }
}

extension FillInTheBlanksVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateLeftAlignedLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == collectionViewFillInBlanks ? arrFillInBlanks.count : arrOptionModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewFillInBlanks {
            let fillInTheBlanksModel = arrFillInBlanks[indexPath.row]
            if fillInTheBlanksModel.isEditable { // textfiend
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FillInBlanksCollectionCell", for: indexPath) as! FillInBlanksCollectionCell
                if let sequence = arrFillInBlanks[indexPath.row].sequence {
                    cell.textField.tag = sequence
                    cell.textField.placeholder = arrFillInBlanks[indexPath.row].placeholder
                    cell.textField.text = arrFillInBlanks[indexPath.row].word
                    if showCorrectAns {
                        cell.textField.backgroundColor = UIColor.green.withAlphaComponent(0.3)
                    } else {
                        cell.textField.backgroundColor = selectedSequence == sequence ? UIColor.blue.withAlphaComponent(0.3) : UIColor.gray.withAlphaComponent(0.1)
                    }
                }
                return cell
            } else { // label
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
                cell.setupData(text: arrFillInBlanks[indexPath.row].word)
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
            let optionModel = arrOptionModel[indexPath.row]
            cell.setupData(text: optionModel.option.validate)
            cell.label.backgroundColor = optionModel.isSelected ? UIColor.black : UIColor.lightGray
            cell.label.textColor = optionModel.isSelected ? UIColor.white : UIColor.blue
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 25.0)
        if collectionView == collectionViewFillInBlanks {
            if arrFillInBlanks.indices.contains(indexPath.row) {
                size.width = arrFillInBlanks[indexPath.row].wordWidth
            }
        } else {
            size.width = (collectionViewOptions.frame.size.width * 0.333334) - 5 /*arrOptionModel[indexPath.row].option.validate.sizeOf(UIFont.systemFont(ofSize: 15.0)).width + 11.0*/
            size.height = 40.0
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == collectionViewFillInBlanks ? 3 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == collectionViewFillInBlanks ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewFillInBlanks {
            if let sequence = arrFillInBlanks[indexPath.row].sequence {
                selectedSequence = sequence
                collectionViewFillInBlanks.reloadDataInMain()
            }
        } else {
            let selectedOptionText = arrOptionModel[indexPath.row].option.validate
            arrOptionModel[indexPath.row].isSelected = true
            collectionViewOptions.reloadDataInMain()
            
            if let sequenceIndex = arrFillInBlanks.index(where: { (fillInBlanks) -> Bool in
                return fillInBlanks.sequence == selectedSequence
            }) {
                selectedSequence += 1
              /*  for i in 0 ..< arrFillInBlanks.count {
                    if arrFillInBlanks[i].word == selectedOptionText, sequenceIndex != i {
                        arrFillInBlanks[i].word = ""
                    }
                }*/ // comment by hp
                arrFillInBlanks[sequenceIndex].word = selectedOptionText
                arrFillInBlanks[sequenceIndex].id = arrOptionModel[indexPath.row].id.validate
                collectionViewFillInBlanks.reloadDataInMain()
            }
        }
        updateOptionCollectionView()
    }
    
    func updateOptionCollectionView() {
        let filteredArray = arrFillInBlanks.filter({ $0.sequence != nil })
        for i in 0 ..< arrOptionModel.count {
                arrOptionModel[i].isSelected = false
        }
        for fillInTheBlanks in filteredArray{
            for i in 0 ..< arrOptionModel.count {
                if arrOptionModel[i].option == fillInTheBlanks.word {
                    arrOptionModel[i].isSelected = true
                }
            }
        }
        collectionViewOptions.reloadDataInMain()
    }
}




extension FillInTheBlanksVC {
    //MARK:- ans api call
    func callAnsAPI() {
         
         if let testDetailVC = self.parent as? MockTestDetailVC {
             
             if let mockQestionModel = self.mockQestionModel {
                 let sectionTime = testDetailVC.totalTime - timeOnScreen
                 
                 let filteredArray = arrFillInBlanks.filter({ $0.sequence != nil }).sorted { (obj1, obj2) -> Bool in
                     return obj1.sequence! < obj2.sequence!
                 }
                 
                 print(">>> \(filteredArray)")
                 
                 let optionid = filteredArray.map({ $0.id.count > 0 ? $0.id : "" }).joined(separator: ", ")
                 print("optionid >>>>> \(optionid)")
                 
                 print(sectionTime)
                 
                 let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" ,"user_id": "\(AppDataManager.shared.loginData.id!)", "reading_section_timer" : "\(Int(sectionTime))", "optionid": optionid]
                 
                 print(">>> \(param)")
                 
                 testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.readingResult.rawValue)
                 
             }
             
         }
     }
}
