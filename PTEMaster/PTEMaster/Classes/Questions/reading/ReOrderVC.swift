//
//  ReOrderVC.swift
//  PTEMaster
//
//  Created by mac on 04/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class ReOrderVC: UIViewController {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var tblAnswere: UITableView!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    
    var arrOptionModel = [OptionsModel]()
    var timeOnScreen: TimeInterval = 0
    var count : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count  = 0
        setTableView()
        setupData()
    }
    
    func setTableView() {
        tblAnswere.delegate = self
        tblAnswere.dataSource = self
        tblAnswere.estimatedRowHeight = 50
        tblAnswere.rowHeight = UITableView.automaticDimension
        tblAnswere.isEditing = true
        
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
        }else if let mockQestionModel = practiceTestModel{
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
        }
        
        let new = arrOptionModel.shuffled()
        arrOptionModel = new
        // consTblHeight.constant = CGFloat(arrOptionModel.count) * UITableView.automaticDimension
    }
    
    func updateTimer() {
        timeOnScreen += 1
    }
    
    //MARK:- ans api call
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC {
            if let mockQestionModel = self.mockQestionModel {
                
                //[ ids jis sequence me h wo sequence me pass krna h ]  yha pr
                let selectAnsId = arrOptionModel.map(({$0.id.validate})).joined(separator: ",")
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                
                let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" ,"user_id": "\(AppDataManager.shared.loginData.id!)", "reading_section_timer" : "\(Int(sectionTime))" , "optionid": selectAnsId]
                
                testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.readingResult.rawValue)
            }
            
        }
    }
    
}


extension ReOrderVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAnswere.dequeueReusableCell(withIdentifier: "ReadingAnswereCell", for: indexPath) as! ReadingAnswereCell
        // option list random krke show krna h list me ......
        cell.lblAns.text = arrOptionModel[indexPath.row].option ?? "N/A"
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let movedObject = self.arrOptionModel[sourceIndexPath.row]
        arrOptionModel.remove(at: sourceIndexPath.row)
        arrOptionModel.insert(movedObject, at: destinationIndexPath.row)
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if count  != arrOptionModel.count{
             count += 1
        let animation = AnimationFactory.makeSlideIn(duration: 0.2, delayFactor: 0.02)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
}


