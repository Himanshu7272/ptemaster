//
//  ReadWriteBlanksVC.swift
//  PTEMaster
//
//  Created by mac on 04/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class ReadWriteBlanksVC: UIViewController {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var collectionViewFillInBlanks: UICollectionView!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?

    var arrOptionModel = [OptionsModel]()
    var arrFillInTheBlanks = [FillInBlanksModel]()

    var timeOnScreen: TimeInterval = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupData()
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            setupDescriptionCollectionView(mockQestionModel.description.validate.removeHtmlFromString())
        } else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            setupDescriptionCollectionView(mockQestionModel.description.validate.removeHtmlFromString())
        }
    }
    
    func setupDescriptionCollectionView(_ description: String) {
        let strSeprated = description.components(separatedBy: " ")
        let differentSequences = Array(Set(arrOptionModel.compactMap({ $0.sequence })))
        for i in 0 ..< strSeprated.count {
            if strSeprated[i].trimWhitespaces.count > 0 {
                var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "")
                for sequence in differentSequences {
                    let strSequence = "{\(sequence)}"
                    if strSeprated[i].range(of: strSequence) != nil {
                        fillInTheBlanks.word = " _\(sequence)_ "
                        fillInTheBlanks.sequence = Int(sequence) ?? 0
                        fillInTheBlanks.correctAns = arrOptionModel.filter({ $0.sequence == sequence && $0.correct == "1" }).first?.option ?? ""
                        fillInTheBlanks.id = ""
                    }
                }
                arrFillInTheBlanks.append(fillInTheBlanks)
            }
        }
        collectionViewFillInBlanks.reloadDataInMain()
    }
    
    
    func updateTimer() {
        timeOnScreen += 1
    }
    
    //MARK:- ans api call
    
    func callAnsAPI() {
        
        if let testDetailVC = self.parent as? MockTestDetailVC {
            if let mockQestionModel = self.mockQestionModel {
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                print(sectionTime)
                var newOptionIds = [String]()
                for fillInTneBlanks in arrFillInTheBlanks {
                    if let sequence = fillInTneBlanks.sequence {
                        if fillInTneBlanks.word == " _\(sequence)_ " {
                            //newOptionIds.append("null")
                            newOptionIds.append("")
                        } else {
                            //newOptionIds.append(fillInTneBlanks.word)
                            newOptionIds.append(fillInTneBlanks.id)
                        }
                    }
                }
                let ids = newOptionIds.joined(separator: ", ")
                print(" new ids >>> \(ids)")
                
                let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" ,"user_id": "\(AppDataManager.shared.loginData.id!)", "reading_section_timer" : "\(Int(sectionTime))" , "optionid": "\(ids)"]
                
                testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.readingResult.rawValue)
                
            }
        }
    }
    
  /* func checkAns() {
        for i in 0 ..< arrFillInTheBlanks.count {
            if arrFillInTheBlanks[i].sequence != nil {
                arrFillInTheBlanks[i].word = arrFillInTheBlanks[i].correctAns
                arrFillInTheBlanks[i].displayCorrectAns = true
            }
        }
        collectionViewFillInBlanks.reloadDataInMain()
    } */
    
    func checkAns() {
       
        var answers = [CheckAnswerFTBModel]()
        for i in 0 ..< arrFillInTheBlanks.count {
            if arrFillInTheBlanks[i].sequence != nil {
                var answer = CheckAnswerFTBModel(correctAnswer: "", userAnswer: arrFillInTheBlanks[i].word)
                answer.correctAnswer = arrFillInTheBlanks[i].correctAns
                if answer.userAnswer.count == 0 || answer.userAnswer.trimWhitespaces == "_\(arrFillInTheBlanks[i].sequence!)_" {
                    answer.userAnswer = "-"
                }
                answers.append(answer)
            }
        }
        CheckAnswerFillInTheBlanksVC.open(in: self, answers: answers)
    }
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}


extension ReadWriteBlanksVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateLeftAlignedLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFillInTheBlanks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fillInTheBlanksModel = arrFillInTheBlanks[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
        cell.setupData(text: fillInTheBlanksModel.word)
        cell.label.textColor = fillInTheBlanksModel.sequence == nil ? UIColor.black : (fillInTheBlanksModel.displayCorrectAns ? UIColor.green : UIColor.blue)
        
        // cell.label.textColor = fillInTheBlanksModel.sequence == nil ? UIColor.black :  UIColor.green
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      /*  if isCheckingAns {
            return
        }*/
         // isCheckingAns = false
        
        let fillInTheBlanksModel = arrFillInTheBlanks[indexPath.row]
        if let sequence = fillInTheBlanksModel.sequence {
            let filteredOptions = arrOptionModel.filter({ $0.sequence == "\(sequence)" })
            if filteredOptions.count > 0 {
                OptionPopupView.showBankAlertPopup(in: self, options: filteredOptions) { (selectedText, selectedId) in
                    if selectedText.count != 0, selectedId.count != 0 {
                        self.arrFillInTheBlanks[indexPath.row].word = selectedText
                        self.arrFillInTheBlanks[indexPath.row].id = selectedId
                            self.arrFillInTheBlanks[indexPath.row].displayCorrectAns = false
                        self.collectionViewFillInBlanks.reloadDataInMain()
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 22.0)
        if arrFillInTheBlanks.indices.contains(indexPath.row) {
            size.width = arrFillInTheBlanks[indexPath.row].wordWidth + 8
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
