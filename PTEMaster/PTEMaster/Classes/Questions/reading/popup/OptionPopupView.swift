//
//  OptionPopupView.swift
//  PTEMaster
//
//  Created by mac on 05/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class OptionPopupView: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var consTableViewOptnHeight: NSLayoutConstraint!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tableViewOptn: UITableView!

    var dismissBlock: ((String, String) -> Void)?
    var optionModules = [OptionsModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.startAnimation()
        }
    }
    
    func initialSetup() {
        popUpView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
        popUpView.alpha = 0.3
        view.backgroundColor = UIColor.clear
        DispatchQueue.main.async {
            self.updateTableContentHeight()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionModules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell") as! OptionCell
    
        cell.lblOptionTitle.text = optionModules[indexPath.row].option.validate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dismissWithAnimation(optionModules[indexPath.row].option.validate, id: optionModules[indexPath.row].id.validate)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func updateTableContentHeight() {
        var height = CGFloat(40 * optionModules.count) + 15
        let maxHeight = ScreenSize.height - 100
        if height > maxHeight {
            height = maxHeight
        }
        DispatchQueue.main.async {
            self.consTableViewOptnHeight.constant = height
        }
    }
   
    func notify(_ title: String, id: String) {
        if let block = self.dismissBlock {
            block(title, id)
        }
    }
    
    static func showBankAlertPopup(in viewController: UIViewController, options: [OptionsModel], completion:  @escaping (String, String) -> Void) {
        
        let obj = OptionPopupView.instance(.question) as! OptionPopupView
        obj.dismissBlock = completion
        obj.optionModules = options
        obj.modalPresentationStyle = .custom
        viewController.present(obj, animated: false, completion: nil)
    }
    
    
    @IBAction func dismissPopUp(_ sender: Any)  {
        
        dismissWithAnimation("", id: "")
        
    }
    
    
    func startAnimation() {
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            self.popUpView.alpha = 0.8
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        }) { (completed) in
            UIView.animate(withDuration: 0.3, animations: {
                self.popUpView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
                self.popUpView.alpha = 1.0
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            })
        }
    }
    
    func dismissWithAnimation(_ title: String, id: String) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
            self.popUpView.alpha = 0.3
            self.view.backgroundColor = UIColor.clear
        }) { (completed) in
            self.dismiss(animated: false, completion: {
                self.notify(title, id: id)
            })
        }
    }

}


class OptionCell: UITableViewCell {
    
    @IBOutlet weak var lblOptionTitle: UILabel!
    
}
