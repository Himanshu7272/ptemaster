//
//  ShortAnswereVC.swift
//  PTEMaster
//
//  Created by mac on 28/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class ShortAnswereVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnReplay: UIButton!
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?

    var waitTime: TimeInterval = 0
    var responseTime: TimeInterval = 0
    var screenStatus = ScreenStatus.preparingToPlay
    var timeOnScreen: TimeInterval = 0
    var recordTime: TimeInterval = 0
    var timesPause = Int64()
    var prev_length:Int = 0
    var t1 = Int64()
    var t0 = Int64()
    var pausetime:Int64 = 0
    var abbs = Double()
    var pausefinaltime:Int64 = 0
    
    var isPracticeTest: Bool {
        return practiceTestModel != nil
    }
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    // pre audio
    private var playerItemContext = 0
    
    // pre audio
    enum ScreenStatus {
        case preparingToPlay
        case waiting
        case playingAudio
        case recognizingText
        case ideal
    }
    
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        // pre audio
        textView.delegate = self
        setupData()
        speechRecognizer.delegate = self
        checkForSpeechRecognizer()
        setupSaveButtonTitle(true)
    }
    
    //stop audio
    override func viewDidDisappear(_ animated: Bool) {
        stopPlaying()
    }
    
    func checkForSpeechRecognizer() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            break
        case .denied:
            print("User denied access to speech recognition")
        case .restricted:
            print("Speech recognition restricted on this device")
        case .notDetermined:
            askForSpeechRecognizer()
            print("Speech recognition not yet authorized")
        }
    }
    
    func askForSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            self.checkForSpeechRecognizer()
        }
    }
    
    func setupData() {
        
        btnReplay.isHidden = !isPracticeTest
        DispatchQueue.main.async {
            self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
        }
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        if let mockQestionModel = self.mockQestionModel {
            //audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            //audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            // Pre-audio
        }else if let mockQestionModel = practiceTestModel {
            //audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            //audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
        }
    }
    
    func updateTimer(text: String) {
        timeOnScreen += 1
        if SFSpeechRecognizer.authorizationStatus() == .authorized {
            if screenStatus == .waiting {
                constBottomViewHeight.constant = 50
                waitTime -= 1
                if waitTime == 1 {
                    // Pre-audio
                    screenStatus = .playingAudio
                    playAudio()
                    // Pre-audio
                    // https://s3.amazonaws.com/kargopolov/kukushka.mp3
                }
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
                self.setupSaveButtonTitle(true)
            } else if screenStatus == .playingAudio {
                constBottomViewHeight.constant = 0
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "1", thirdStr: " seconds")
                self.setupSaveButtonTitle(true)
            } else if screenStatus == .recognizingText {
                constBottomViewHeight.constant = 50
                responseTime -= 1
                if responseTime == 0 {
                    stopRecordingAndCallAnsAPI()
                }
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
                self.setupSaveButtonTitle(!audioEngine.isRunning)
            } else {
                lblTimeLeft.text = ""
                if practiceTestModel != nil {
                    constBottomViewHeight.constant = 0
                }
            }
        }
    }
    
    func setupSaveButtonTitle(_ setSkip: Bool) {
        self.btnSkipOrSave.setTitle(setSkip ? "    SKIP    " : "    STOP & SAVE    ", for: .normal)
    }
    
    //MARK:- ans api call
    func stopRecordingAndCallAnsAPI() {
        stopRecording()
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" , "new_name" : "test" , "user_id" : "\(AppDataManager.shared.loginData.id!)" , "recordduration" : " calculate record time " , "usetxt" : "\(textView.text!)"  , "confidance" : "1" , "speaking_section_timer" : "\(sectionTime)"]
            testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
        }else if let practice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
            let sectionTime = practice.totalTime - timeOnScreen
            print(sectionTime)
            
            let recordDuration = questionModal.responseTime - recordTime
            print(recordDuration)
            
            let param : [String: Any] = ["question_id": "\(questionModal.id!)"  , "user_id" : "\(AppDataManager.shared.loginData.id!)", "usetxt" : "\(textView.text!)"  , "abbss":abbs , "mocqdesc":questionModal.description as Any,"question_cat":questionModal.categoryId as Any]
            practice.callAnswerAPIWithParameters(param, ansApi: RequestPath.practice_speaking_mobile.rawValue)
            
        }  else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        view.layoutSubviews()
    }
    

    // MARK:- button action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .waiting {
            playAudio()
        } else if screenStatus == .recognizingText {
            stopRecordingAndCallAnsAPI()
        }
    }
    
    @IBAction func btnRepeatAudioAction(_ sender: Any) {
        
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
}


extension ShortAnswereVC: AVAudioPlayerDelegate {
    
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD("Prepare audio", onView: self.view)
            avPlayerItem = AVPlayerItem(url: url)
            avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
            audioPlayer = AVPlayer(playerItem: avPlayerItem)
            audioPlayer?.pause()
        }
    }
    
    // MARK:- Pre-audio
    func playAudio() {
        if self.screenStatus == .playingAudio {
            constBottomViewHeight.constant = 0
            pauseAndStartTimer(true)
            audioPlayer?.play()
            let interval = CMTime(value: 1, timescale: 2)
            audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                let seconds = CMTimeGetSeconds(progressTime)
                if let duration = self.audioPlayer?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let progress = Float(seconds / durationSeconds)
                    self.audioProgress.progress = progress
                    if progress >= 1 {
                        self.pauseAndStartTimer(false)
                        self.constBottomViewHeight.constant = 50
                        self.updateScreenWhenAudioStops()
                    }
                }
            })
        }
    }
    
    
    // MARK:- Pre-audio
    func stopPlaying() {
        audioPlayer?.pause()
        audioPlayer = nil
        avPlayerItem = nil
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying()
        screenStatus = .recognizingText
        startRecording()
    }
    
    // MARK:- Pre-audio
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            pauseAndStartTimer(false)
            screenStatus = .waiting
            appDelegate.hideHUD(self.view)
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
}

extension ShortAnswereVC : SFSpeechRecognizerDelegate {
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        let inputNode = audioEngine.inputNode  //4
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            if result != nil {
                self.textView.text = result?.bestTranscription.formattedString  //9
                let tsLong = Date.currentTimeStamp
                print("\(tsLong)")
                let length_of_data = result?.bestTranscription.formattedString.count
                let abbs = self.isCalulationPulseTime(length_of_data ?? 0 , tsLong)
                print(abbs)
                isFinal = (result?.isFinal)!
                
            }
            
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
        if length_of_data > 0 {
            if self.t0 != 0 {
                prev_length = length_of_data;
                t1 = Date.currentTimeStamp
                print("fluency_t1", t1)
                pausetime = (t1 - t0);
                print("fluency_pausetime", pausetime)
                t0 = t1
                if (pausetime >= 450 && pausetime <= 550) {
                    abbs = abbs + 0.1
                    print("abs_live_value", abbs)
                } else if (pausetime > 551 && pausetime <= 590) {
                    abbs = abbs + 0.2
                    print("abs_live_value", abbs)
                } else if (pausetime >= 591 && pausetime <= 650) {
                    abbs = abbs + 0.3
                    print("abs_live_value", abbs)
                } else if (pausetime >= 651 && pausetime <= 690) {
                    abbs = abbs + 0.4
                    print("abs_live_value",  abbs)
                } else if (pausetime >= 691 && pausetime <= 750) {
                    abbs = abbs + 0.5
                    print("abs_live_value", abbs)
                } else if (pausetime >= 751) {
                    abbs = abbs + 1
                    print("abs_live_value", abbs)
                }
                print("pausetimecalc",  pausetime)
                print("fluency_abbs",  abbs)
                print("fluency_finalpause",  pausetime)
                pausefinaltime = pausetime
                print("pause_len_of_data", length_of_data)
                print("pause_prev_len",  prev_length)
            } else {
                t0 = Date.currentTimeStamp
                t1 = t0
            }
        }  else if (length_of_data == prev_length) {
            print("pausefinaltime", pausetime)
            print("pause_len_of_data", length_of_data)
            print("pause_prev_len", prev_length)
        }
        return abbs
    }
    
    func stopRecording() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphoneButton.isEnabled = true
        } else {
            //microphoneButton.isEnabled = false
        }
    }
    
}

