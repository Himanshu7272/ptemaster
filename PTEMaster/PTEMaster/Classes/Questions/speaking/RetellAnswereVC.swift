//
//  RetellAnswereVC.swift
//  PTEMaster
//
//  Created by mac on 07/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class RetellAnswereVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var btnReplay: UIButton!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
    @IBOutlet weak var viewRecording: UIView!
    @IBOutlet weak var btnStartStopRecording: UIButton!
    @IBOutlet weak var lblRecordingHint: UILabel!
    @IBOutlet weak var lblPracticeTimer: UILabel!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    
    var waitTime: TimeInterval = 0
    var responseTime: TimeInterval = 0
    var screenStatus = ScreenStatus.preparingToPlay
    var timeOnScreen: TimeInterval = 0
    var recordTime: TimeInterval = 0
    var timesPause = Int64()
    var prev_length:Int = 0
    var t1 = Int64()
    var t0 = Int64()
    var pausetime:Int64 = 0
    var abbs = Double()
    var pausefinaltime:Int64 = 0
    
    
    var isPracticeTest: Bool {
        return practiceTestModel != nil
    }
    
    let speechRecognizer = SpeechRecognizerManager()
    let audioPlayerManager = AudioPlayerManager()
    
    // pre audio
    enum ScreenStatus {
        case preparingToPlay
        case waiting
        case playingAudio
        case recognizingText
        case ideal
    }
    
    /* private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
     private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
     private var recognitionTask: SFSpeechRecognitionTask?
     private let audioEngine = AVAudioEngine()
     
     // pre audio
     private var playerItemContext = 0
     
     // pre audio
     enum ScreenStatus {
     case preparingToPlay
     case waiting
     case playingAudio
     case recognizingText
     case ideal
     }
     
     var audioPlayer: AVPlayer?
     var avPlayerItem: AVPlayerItem?*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textView.delegate = self
        setupData()
        checkForSpeechRecognizer()
        setupSaveButtonTitle(true)
        
        if isPracticeTest {
            speechRecognizer.stopRecognizingAfter5Sec = true
            speechRecognizer.recognizingStopAfter5SecBlock = {
                self.pauseAndStartTimer(true)
                self.btnStartStopRecording.isSelected = false
            }
        }
    }
    
    
    // stop audio
    override func viewDidDisappear(_ animated: Bool) {
        stopPlaying(true)
    }
    
    func checkForSpeechRecognizer() {
        speechRecognizer.checkForSpeechRecognizer()
    }
    
    func setupData() {
        
        btnReplay.isHidden = !isPracticeTest
        DispatchQueue.main.async {
            self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
        }
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            // Pre-audio
        }else if let mockQestionModel = practiceTestModel{
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
        }
    }
    
    func updateTimer(text: String) {
        
        // **********   rtime left wala label show nhi ho rha
        
        print("screenStatus >>>>>> \(screenStatus)")
        
        timeOnScreen += 1
        if speechRecognizer.isAuthorized {
            if screenStatus == .waiting {
                constBottomViewHeight.constant = isPracticeTest ? 0 : 50
                waitTime -= 1
                if waitTime == 1 {
                    screenStatus = .playingAudio
                    playAudio()
                }
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
                self.setupSaveButtonTitle(true)
            } else if screenStatus == .playingAudio {
                constBottomViewHeight.constant = 0
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "1", thirdStr: " seconds")
                self.setupSaveButtonTitle(true)
            } else if screenStatus == .recognizingText {
                if speechRecognizer.isRunning {
                    recordTime += 1
                }
                constBottomViewHeight.constant = isPracticeTest ? 0 : 50
                responseTime -= 1
                if responseTime == 0 {
                    stopRecordingAndCallAnsAPI()
                }
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
                self.setupSaveButtonTitle(!speechRecognizer.isRunning)
            } else {
                lblTimeLeft.text = ""
                if practiceTestModel != nil {
                    constBottomViewHeight.constant = 0
                }
            }
            updatePracticeBottomView()
            
        }
    }
    
    func updatePracticeBottomView() {
        if isPracticeTest {
            if screenStatus == .waiting {
                lblPracticeTimer.text = "\(Int(waitTime))"
                lblPracticeTimer.textColor = UIColor.green
                viewRecording.isHidden = false
            } else if screenStatus == .playingAudio {
                viewRecording.isHidden = true
            } else if screenStatus == .recognizingText {
                viewRecording.isHidden = false
                lblPracticeTimer.textColor = UIColor.red
                lblPracticeTimer.text = "\(Int(responseTime))"
                if responseTime <= 1 {
                    viewRecording.isHidden = true
                }
            }
        }
    }
    
    func setupSaveButtonTitle(_ setSkip: Bool) {
        self.btnSkipOrSave.setTitle(setSkip ? "    SKIP    " : "    STOP & SAVE    ", for: .normal)
    }
    
    //MARK:- ans api call
    func stopRecordingAndCallAnsAPI() {
        stopRecording()
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            // **********
            // recordduration -- calculate record time [time left me se jo time chla gya wo - hokr send krna h]
            
            let recordDuration = mockQestionModel.responseTime - recordTime
            print(recordDuration)
            
            let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" , "new_name" : "test" , "user_id" : "\(AppDataManager.shared.loginData.id!)" , "recordduration" : "\(Int(recordDuration))" , "usetxt" : "\(textView.text!)"  , "confidance" : "1" , "speaking_section_timer" : "\(sectionTime)"]
            testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
        } else if let practice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
            let sectionTime = practice.totalTime - timeOnScreen
            print(sectionTime)
            
            let recordDuration = questionModal.responseTime - recordTime
            print(recordDuration)
            
            let param : [String: Any] = ["question_id": "\(questionModal.id!)"  , "user_id" : "\(AppDataManager.shared.loginData.id!)", "usetxt" : "\(textView.text!)"  , "abbss":abbs , "mocqdesc":questionModal.description as Any,"question_cat":questionModal.categoryId as Any]
            practice.callAnswerAPIWithParameters(param, ansApi: RequestPath.practice_speaking_mobile.rawValue)
            
        }  else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        view.layoutSubviews()
    }
    
    
    
    // MARK:- button action
    @IBAction func btnRepeatAudioAction(_ sender: Any) {
        replay()
    }
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .waiting {
            playAudio()
        } else if screenStatus == .recognizingText {
            stopRecordingAndCallAnsAPI()
        }
    }
    
    @IBAction func btnStartStopRecording(_ sender: Any) {
        if speechRecognizer.state == .recognizing {
            pauseAndStartTimer(true)
            speechRecognizer.pauseRecording()
            btnStartStopRecording.isSelected = false
        } else if speechRecognizer.state == .paused {
            pauseAndStartTimer(false)
            btnStartStopRecording.isSelected = true
            speechRecognizer.reStartRecording()
        } else if speechRecognizer.state == .ideal || speechRecognizer.state == .stoped {
            pauseAndStartTimer(false)
            btnStartStopRecording.isSelected = true
            startRecording()
        }
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
}


extension RetellAnswereVC: AVAudioPlayerDelegate {
    
    // MARK:- Pre-audio
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD("Prepare audio", onView: self.view)
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .waiting
                appDelegate.hideHUD(self.view)
            }
        }
    }
    
    // MARK:- Pre-audio
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            constBottomViewHeight.constant = 0
            playAudioAndUpdateTimer()
            viewRecording.isHidden = true
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
        self.audioProgress.progress = progress
        if progress >= 1 {
            self.pauseAndStartTimer(false)
            self.constBottomViewHeight.constant = isPracticeTest ? 0 : 50
            self.updateScreenWhenAudioStops()
            self.viewRecording.isHidden = false
        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    
    // MARK:- Pre-audio
    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
    func replay() {
        stopRecording()
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying(!isPracticeTest)
        //if !isPracticeTest {
        screenStatus = .recognizingText
        startRecording()
        //}
    }
    
    func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
           if length_of_data > 0 {
               if self.t0 != 0 {
                   prev_length = length_of_data;
                   t1 = Date.currentTimeStamp
                   print("fluency_t1", t1)
                   pausetime = (t1 - t0);
                   print("fluency_pausetime", pausetime)
                   t0 = t1
                   if (pausetime >= 450 && pausetime <= 550) {
                       abbs = abbs + 0.1
                       print("abs_live_value", abbs)
                   } else if (pausetime > 551 && pausetime <= 590) {
                       abbs = abbs + 0.2
                       print("abs_live_value", abbs)
                   } else if (pausetime >= 591 && pausetime <= 650) {
                       abbs = abbs + 0.3
                       print("abs_live_value", abbs)
                   } else if (pausetime >= 651 && pausetime <= 690) {
                       abbs = abbs + 0.4
                       print("abs_live_value",  abbs)
                   } else if (pausetime >= 691 && pausetime <= 750) {
                       abbs = abbs + 0.5
                       print("abs_live_value", abbs)
                   } else if (pausetime >= 751) {
                       abbs = abbs + 1
                       print("abs_live_value", abbs)
                   }
                   print("pausetimecalc",  pausetime)
                   print("fluency_abbs",  abbs)
                   print("fluency_finalpause",  pausetime)
                   pausefinaltime = pausetime
                   print("pause_len_of_data", length_of_data)
                   print("pause_prev_len",  prev_length)
               } else {
                   t0 = Date.currentTimeStamp
                   t1 = t0
               }
           }  else if (length_of_data == prev_length) {
               print("pausefinaltime", pausetime)
               print("pause_len_of_data", length_of_data)
               print("pause_prev_len", prev_length)
           }
           return abbs
       }
}

extension RetellAnswereVC : SFSpeechRecognizerDelegate {
    
    
    func startRecording() {
        btnStartStopRecording.isSelected = true
        speechRecognizer.startRecording { (text) in
            self.textView.text = text
            let tsLong = Date.currentTimeStamp
            print("\(tsLong)")
            let length_of_data = text.count
            let abbs = self.isCalulationPulseTime(length_of_data , tsLong)
            print(abbs)
        }
    }
    
    func stopRecording() {
        speechRecognizer.stopRecording()
    }
}
