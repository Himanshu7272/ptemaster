//
//  DescribeImageVC.swift
//  PTEMaster
//
//  Created by mac on 25/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech

class DescribeImageVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var imgQuestion: UIImageView!
    @IBOutlet weak var consHeightImg: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var constViewBottomHeight: NSLayoutConstraint!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var waitTime: TimeInterval = 0
    var responseTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var screenStatus = ScreenStatus.waiting
    var recordTime: TimeInterval = 0
    var timesPause = Int64()
    var prev_length:Int = 0
    var t1 = Int64()
    var t0 = Int64()
    var pausetime:Int64 = 0
    var abbs = Double()
    var pausefinaltime:Int64 = 0

    enum ScreenStatus {
        case waiting
        case recording
        case ideal
    }

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textView.delegate = self
        setupData()
        speechRecognizer.delegate = self
        checkForSpeechRecognizer()
        setupSaveButtonTitle(true)
    }
    
    //stop audio
    override func viewDidDisappear(_ animated: Bool) {
       // stopPlaying()
    }
    
    func checkForSpeechRecognizer() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            break
        case .denied:
            print("User denied access to speech recognition")
        case .restricted:
            print("Speech recognition restricted on this device")
        case .notDetermined:
            askForSpeechRecognizer()
            print("Speech recognition not yet authorized")
        }
    }
    
    func askForSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            self.checkForSpeechRecognizer()
        }
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            if let url = URL(string: BaseUrlTestQuestion  + mockQestionModel.testId.validate + "/" + mockQestionModel.image.validate) {
                
                imgQuestion.kf.indicatorType = .activity
                imgQuestion.kf.setImage(with: url) { result in
                    switch result {
                    case .success(let value):
                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            }
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            
        }else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            if let url = URL(string: BaseUrlTestQuestion  + mockQestionModel.testId.validate + "/" + mockQestionModel.image.validate) {
                
                imgQuestion.kf.indicatorType = .activity
                imgQuestion.kf.setImage(with: url) { result in
                    switch result {
                    case .success(let value):
                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            }
            
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
        }
    }
    
    func updateTimer() {
        timeOnScreen += 1
        if SFSpeechRecognizer.authorizationStatus() == .authorized {
            if screenStatus == .waiting  {
                waitTime -= 1
                screenStatus = waitTime > 0 ? .waiting : .recording
                if screenStatus == .recording, !audioEngine.isRunning {
                    startRecording()
                }
            } else if screenStatus == .recording {
                
                if audioEngine.isRunning {
                    recordTime += 1
                }
                responseTime -= 1
                if responseTime == 0 {
                    stopRecordingAndCallAnsAPI()
                }
            }
            
            if screenStatus == .waiting {
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
            } else if responseTime > 0, screenStatus == .recording {
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
            } else {
                lblTimeLeft.text = ""
                if practiceTestModel != nil {
                    constViewBottomHeight.constant = 0
                }
            }
            self.setupSaveButtonTitle(!audioEngine.isRunning)
        }

    }
    
    func setupSaveButtonTitle(_ setSkip: Bool) {
        self.btnSkipOrSave.setTitle(setSkip ? "    SKIP    " : "    STOP & SAVE    ", for: .normal)
    }
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        view.layoutSubviews()
    }
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
       if screenStatus == .waiting {
            screenStatus = .recording
            startRecording()
        } else {
            if audioEngine.isRunning, screenStatus == .recording {
                stopRecordingAndCallAnsAPI()
            }
        }
       /* if screenStatus == .waiting {
            playAudio()
        } else if screenStatus == .recognizingText {
            stopRecordingAndCallAnsAPI()
        }*/
    }
    
}

extension DescribeImageVC: SFSpeechRecognizerDelegate {
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        let inputNode = audioEngine.inputNode  //4
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            
            if result != nil {
                
                let tsLong = Date.currentTimeStamp
                print("\(tsLong)")
                let length_of_data = result?.bestTranscription.formattedString.count
                let abbs = self.isCalulationPulseTime(length_of_data ?? 0, tsLong)
                print(abbs)
                self.textView.text = result?.bestTranscription.formattedString  //9
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {  //10
                
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
            }
        })

        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
        if length_of_data > 0 {
            if self.t0 != 0 {
                prev_length = length_of_data;
                t1 = Date.currentTimeStamp
                print("fluency_t1", t1)
                pausetime = (t1 - t0);
                print("fluency_pausetime", pausetime)
                t0 = t1
                if (pausetime >= 450 && pausetime <= 550) {
                    abbs = abbs + 0.1
                    print("abs_live_value", abbs)
                } else if (pausetime > 551 && pausetime <= 590) {
                    abbs = abbs + 0.2
                    print("abs_live_value", abbs)
                } else if (pausetime >= 591 && pausetime <= 650) {
                    abbs = abbs + 0.3
                    print("abs_live_value", abbs)
                } else if (pausetime >= 651 && pausetime <= 690) {
                    abbs = abbs + 0.4
                    print("abs_live_value",  abbs)
                } else if (pausetime >= 691 && pausetime <= 750) {
                    abbs = abbs + 0.5
                    print("abs_live_value", abbs)
                } else if (pausetime >= 751) {
                    abbs = abbs + 1
                    print("abs_live_value", abbs)
                }
                print("pausetimecalc",  pausetime)
                print("fluency_abbs",  abbs)
                print("fluency_finalpause",  pausetime)
                pausefinaltime = pausetime
                print("pause_len_of_data", length_of_data)
                print("pause_prev_len",  prev_length)
            } else {
                t0 = Date.currentTimeStamp
                t1 = t0
            }
        }  else if (length_of_data == prev_length) {
            print("pausefinaltime", pausetime)
            print("pause_len_of_data", length_of_data)
            print("pause_prev_len", prev_length)
        }
        return abbs
    }
    
    
    
    
    //MARK:- ans api call
    func stopRecordingAndCallAnsAPI() {
        screenStatus = .ideal
        stopRecording()
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockTestInfo = self.mockQestionModel {
            
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            
            let recordDuration = mockTestInfo.responseTime - recordTime
            print(recordDuration)
            
            let param : [String: Any] = ["question_id": "\(mockTestInfo.id!)", "new_name": "pte","user_id": "\(AppDataManager.shared.loginData.id!)", "recordduration": "\(Int(recordDuration))", "usetxt": "\(textView.text!)", "confidance": "1", "speaking_section_timer": "\(sectionTime)" ]
            
            testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
            
        } else if let practice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
            let sectionTime = practice.totalTime - timeOnScreen
            print(sectionTime)
            
            let recordDuration = questionModal.responseTime - recordTime
            print(recordDuration)
            
            let param : [String: Any] = ["question_id": "\(questionModal.id!)"  , "user_id" : "\(AppDataManager.shared.loginData.id!)", "usetxt" : "\(textView.text!)"  , "abbss":abbs , "mocqdesc":questionModal.description as Any,"question_cat":questionModal.categoryId as Any]
            practice.callAnswerAPIWithParameters(param, ansApi: RequestPath.practice_speaking_mobile.rawValue)
            
        } else {
            if practiceTestModel != nil {
                constViewBottomHeight.constant = 0
            }
        }
    }
    
    func stopRecording() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphoneButton.isEnabled = true
        } else {
            //microphoneButton.isEnabled = false
        }
    }
    
}
