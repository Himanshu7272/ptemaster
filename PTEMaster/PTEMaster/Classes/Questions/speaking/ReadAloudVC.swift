//
//  ReadAloudVC.swift
//  PTEMaster
//
//  Created by mac on 24/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech



class ReadAloudVC: UIViewController , UITextViewDelegate  {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var constViewBottomHeight: NSLayoutConstraint!    // hide bottom view when api call
    
    
    var mockQestionModel: MockQestionModel?//----
    //----
    var practiceTestModel: PracticeQestionModel?
    
    var waitTime: TimeInterval = 0
    var responseTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var recordTime: TimeInterval = 0
    var pauseSeconds: TimeInterval = 0
    var arrPauseTimes =  [Int]()
    var timesPause = Int64()
    var prev_length:Int = 0
    var t1 = Int64()
    var t0 = Int64()
    var pausetime:Int64 = 0
    var abbs = Double()
    //var pausefinaltime:Int64 = 0
    var screenStatus = ScreenStatus.waiting
    
    enum ScreenStatus {
        case waiting
        case recording
        case ideal
    }

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        setupData()
      //  let asdf = PracticeDetailVC()
       // asdf.ReadAloudDelegate = self
        speechRecognizer.delegate = self
        checkForSpeechRecognizer()
        setupSaveButtonTitle(true)
        textView.text = ""
      /* NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "transcript"), object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(self.transcript(_:)), name: NSNotification.Name(rawValue: "transcript"), object: nil)*/

        // handle notification
        
    }
  /* @objc func transcript(_ notification: NSNotification) {
           print(notification.userInfo ?? "")
           if let dict = notification.userInfo as NSDictionary? {
               if let str = dict["transcript"] as? String{
                let decodedData = Data(base64Encoded: str)!
                let decodedString = String(data: decodedData, encoding: .utf8)
                textView.attributedText = decodedString?.htmlToAttributedString
               }
           }
    }*/
    func setTranscriptFromPracticeDetail (_ transcript : String){
             let decodedData = Data(base64Encoded: transcript)!
             let decodedString = String(data: decodedData, encoding: .utf8)
             textView.attributedText = decodedString?.htmlToAttributedString
    }


    
    func checkForSpeechRecognizer() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            break
        case .denied:
            print("User denied access to speech recognition")
        case .restricted:
            print("Speech recognition restricted on this device")
        case .notDetermined:
            askForSpeechRecognizer()
            print("Speech recognition not yet authorized")
        }
    }
    
    func askForSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            self.checkForSpeechRecognizer()
        }
    }
    
    func setupData() {
         //for mock
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.description?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
        }
        //for practice
        else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.description?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
        }
    }

    func updateTimer() {
        timeOnScreen += 1
     //   print("updateTimer--ReadAloudVC--\(timeOnScreen)")
        if SFSpeechRecognizer.authorizationStatus() == .authorized {
            if screenStatus == .waiting {
                waitTime -= 1
                screenStatus = waitTime > 0 ? .waiting : .recording
                if screenStatus == .recording, !audioEngine.isRunning {
                    startRecording()
                }
            } else if screenStatus == .recording {
                
                if audioEngine.isRunning {
                    recordTime += 1
                }
                responseTime -= 1
                if responseTime == 0 {
                    stopRecordingAndCallAnsAPI()
                }
            }
            
            if screenStatus == .waiting {
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
            } else if responseTime > 0, screenStatus == .recording {
               // print(" responce time ...... \(Int(responseTime)) ")
                pauseSeconds += 1
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
            } else {
                lblTimeLeft.text = ""
                if practiceTestModel != nil {
                    constViewBottomHeight.constant = 0
                }
            }
            self.setupSaveButtonTitle(!audioEngine.isRunning)
        }
    }

    
    func setupSaveButtonTitle(_ setSkip: Bool) {
        self.btnSkipOrSave.setTitle(setSkip ? "    SKIP    " : "    STOP & SAVE    ", for: .normal)
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        view.layoutSubviews()
    }
    
    //MARK:- btn action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .waiting {
            screenStatus = .recording
            startRecording()
        } else {
            if audioEngine.isRunning, screenStatus == .recording {
                stopRecordingAndCallAnsAPI()
            }
        }
    }
    
}

extension ReadAloudVC: SFSpeechRecognizerDelegate {
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2..
       // let speechRecognizer = SFSpeechRecognizer()

        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        
        let inputNode = audioEngine.inputNode  //4
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        speechRecognizer.delegate = self
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            if result != nil {
            
                let currentTimeTS = Date.currentTimeStamp
               // print("\(tsLong)")
                
                let length_of_data = result?.bestTranscription.formattedString.count
              //  print("length_of_data-\(length_of_data)-")
                self.textView.text = result?.bestTranscription.formattedString  //9
                
                let abss = self.isCalulationPulseTime(length_of_data ?? 0, currentTimeTS)
               // print(abss)
                if self.pauseSeconds != 0 {
                    //self.arrPauseTimes.append(self.pauseSeconds)
                }
                 self.pauseSeconds = 0
          //      print(result!.bestTranscription.formattedString as Any)
               // print(" one word and two word distance:-", result?.bestTranscription.segments.distance(from: 1, to: 2) as Any)
                isFinal = (result?.isFinal)!
                
            
            }
            
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        
    }
    
    func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
        //print("length_of_text-\(length_of_data)-")
       // print("CurrentTime----\(timeCurrents)-")
       // print("t0--\(t0)-")
       // print("t1--\(t1)-")
        if length_of_data > 0 {
            if self.t0 != 0 {
                prev_length = length_of_data;
                t1 = Date.currentTimeStamp
              //  print("fluency_t1", t1)
                pausetime = (t1 - t0);
               // print("fluency_pausetime", pausetime)
                t0 = t1
                if (pausetime >= 450 && pausetime <= 550) {
                    abbs = abbs + 0.1
                  //  print("abs_live_value", abbs)
                } else if (pausetime > 551 && pausetime <= 590) {
                    abbs = abbs + 0.2
                   // print("abs_live_value", abbs)
                } else if (pausetime >= 591 && pausetime <= 650) {
                    abbs = abbs + 0.3
                   // print("abs_live_value", abbs)
                } else if (pausetime >= 651 && pausetime <= 690) {
                    abbs = abbs + 0.4
                   // print("abs_live_value",  abbs)
                } else if (pausetime >= 691 && pausetime <= 750) {
                    abbs = abbs + 0.5
                  //  print("abs_live_value", abbs)
                } else if (pausetime >= 751) {
                    abbs = abbs + 1
                  //  print("abs_live_value", abbs)
                }
              //  print("pausetimecalc",  pausetime)
              //  print("fluency_abbs",  abbs)
              //  print("fluency_finalpause",  pausetime)
               // print("pause_len_of_data", length_of_data)
               // print("pause_prev_len",  prev_length)
            } else {
                t0 = Date.currentTimeStamp
                t1 = t0
            }
        }  else if (length_of_data == prev_length) {
           // print("pausefinaltime", pausetime)
           // print("pause_len_of_data", length_of_data)
           // print("pause_prev_len", prev_length)
        }
        return abbs
    }
    
    func stopRecording() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
    }

    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphoneButton.isEnabled = true
            print(available)
        } else {
             print(available)
            //microphoneButton.isEnabled = false
        }
    }
}


extension ReadAloudVC{
    //MARK:- ans api call
    func stopRecordingAndCallAnsAPI() {
        screenStatus = .ideal
        stopRecording()
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
            let mock =  self.parent as? MockTestDetailVC
            
            let sectionTime = mock!.totalTime - timeOnScreen
            print(sectionTime)
            let recordDuration = mockQestionModel.responseTime - recordTime
            print(recordDuration)
            let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" , "new_name" : "test" , "user_id" : "\(AppDataManager.shared.loginData.id!)" , "recordduration" : "\(Int(recordDuration))" , "usetxt" : "\(textView.text!)"  , "confidance" : "1" , "speaking_section_timer" : "\(sectionTime)", "abbss":abbs , "mocqdesc":mockQestionModel.description!,"question_cat":mockQestionModel.categoryId as Any]
            testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
        }
        
        else if let testPractice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
            
            let param : [String: Any] = ["question_id": "\(questionModal.questionId!)"  , "user_id": "\(AppDataManager.shared.loginData.id!)", "usetxt": "\(textView.text!)", "abbss":abbs , "mocqdesc":questionModal.description!, "question_cat":questionModal.categoryId!]
            
            testPractice.callAnswerAPIWithParameters(param, ansApi: RequestPath.practice_speaking_mobile.rawValue)
        } else {
            print("hide view ")
            if practiceTestModel != nil {
                constViewBottomHeight.constant = 0
            }
        }
    }
}








extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}



