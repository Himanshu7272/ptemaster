//
//  HighlightCollectionCell.swift
//  PTEMaster
//
//  Created by mac on 12/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class HighlightCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupData(text: String ) {
        self.label.text = text
    }
}
