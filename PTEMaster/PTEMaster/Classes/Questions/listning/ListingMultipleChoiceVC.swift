//
//  ListingMultipleChoiceVC.swift
//  PTEMaster
//
//  Created by mac on 06/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

class ListingMultipleChoiceVC: UIViewController {
    
    @IBOutlet weak var tblOption: UITableView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var viewSkipOrSaveHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var condTblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnReplay: UIButton!
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
    
    var mockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var practiceTestModel: PracticeQestionModel?
    
    weak var practiceDetailVC: PracticeDetailVC?

    var screenStatus = ScreenStatus.preparingToPlay
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var displayCorrectAns = false


    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
    
    var isPracticeTest: Bool {
        return practiceTestModel != nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        // Do any additional setup after loading the view.
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopPlaying()
    }
    
    func setupData() {
        
        btnReplay.isHidden = !isPracticeTest
        DispatchQueue.main.async {
            self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
        }
        
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = (mockQestionModel.question?.removeHtmlFromString())! + " " + "?"
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            condTblHeight.constant = CGFloat(arrOptionModel.count * 45)
            btnSkipOrSave.isHidden = true

        }else if let mockQestionModel = practiceTestModel {
            //audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            //audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = (mockQestionModel.question?.removeHtmlFromString())! + " " + "?"
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            condTblHeight.constant = CGFloat(arrOptionModel.count * 45)
            btnSkipOrSave.isHidden = true

        }
        print(arrOptionModel.count)
    }
    
    func updateTimer(text: String) {
        print(" update time \(text)")
        timeOnScreen += 1
       if screenStatus == .begining {
            viewSkipOrSaveHeightConstraint.constant = 50
            waitTime -= 1
        
            if waitTime == 0 {
                screenStatus = .playingAudio
                playAudio()
                
                self.viewSkipOrSaveHeightConstraint.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
            
            btnSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
        } else if screenStatus == .playingAudio {
            viewSkipOrSaveHeightConstraint.constant = 0
            viewSkipOrSave.isHidden = true
        }else {
            if practiceTestModel != nil {
                viewSkipOrSaveHeightConstraint.constant = 0
            }
            
        }
    }
    
    
    //MARK:- ans api
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC , let mockQestionModel = self.mockQestionModel {
            
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                print(sectionTime)
                let selectAnsId = arrOptionModel.filter(({$0.isSelected})).map(({$0.id.validate})).joined(separator: ",")
                print(selectAnsId)
                let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(sectionTime)" , "usetxt": "" , "word_count": "" , "optionid": selectAnsId]
            
                testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
            
        }else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                viewSkipOrSaveHeightConstraint.constant = 0
            }
        }
    }
    
    func checkAns() {
        displayCorrectAns = true
        self.tblOption.reloadDataInMain()
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        } else if let practiceDetailVC = self.practiceDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
        viewSkipOrSaveHeightConstraint.constant = 0
        viewSkipOrSave.isHidden = true
    }

}


extension ListingMultipleChoiceVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadingAnswereCell", for: indexPath) as! ReadingAnswereCell
        cell.lblAns.text = arrOptionModel[indexPath.row].option ?? "N/A"
        cell.btnCheck.isSelected = arrOptionModel[indexPath.row].isSelected
        if displayCorrectAns, arrOptionModel[indexPath.row].correct == "1" {
            cell.viewBg.backgroundColor = AppColor.clr1
        }else {
            cell.viewBg.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //for multiple selection
        arrOptionModel[indexPath.row].isSelected = !arrOptionModel[indexPath.row].isSelected
        tblOption.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
}

//MARK:- Audio
extension ListingMultipleChoiceVC {
    
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD("Prepare audio", onView: self.view)
            avPlayerItem = AVPlayerItem(url: url)
            avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
            audioPlayer = AVPlayer(playerItem: avPlayerItem)
            audioPlayer?.pause()
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            audioPlayer?.play()
            let interval = CMTime(value: 1, timescale: 2)
            audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                let seconds = CMTimeGetSeconds(progressTime)
                if let duration = self.audioPlayer?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let progress = Float(seconds / durationSeconds)
                    self.audioProgress.progress = progress
                    if progress >= 1 {
                        self.pauseAndStartTimer(false)
                        self.updateScreenWhenAudioStops()
                    }
                }
            })
        }
    }
    
    func stopPlaying() {
        audioPlayer?.pause()
        audioPlayer = nil
        avPlayerItem = nil
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying()
        //        screenStatus = .recognizingText
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            pauseAndStartTimer(false)
            screenStatus = .begining
            appDelegate.hideHUD(self.view)
            avPlayerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: &playerItemContext)
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
    
}
