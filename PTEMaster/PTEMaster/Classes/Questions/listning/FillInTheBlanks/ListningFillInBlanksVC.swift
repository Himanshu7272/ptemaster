//
//  ListningFillInBlanksVC.swift
//  PTEMaster
//
//  Created by mac on 06/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation


class ListningFillInBlanksVC: UIViewController {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var viewSkipOrSaveHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var collectionViewFillInBlanks: UICollectionView!
    
    
    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    var screenStatus = ScreenStatus.preparingToPlay
    var waitTime: TimeInterval = 0

    var mockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var arrFillInBlanks = [FillInBlanksModel]()
    var timeOnScreen: TimeInterval = 0
    var practiceTestModel: PracticeQestionModel?
    weak var practiceDetailVC: PracticeDetailVC?

    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopPlaying()
    }
    
    func setupData() {
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime

            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }else if let mockQestionModel = self.practiceTestModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
             btnSkipOrSave.isHidden = true
        }
       
        setupCollectionView()
    }
    
    func checkAns() {
        for i in 0 ..< arrFillInBlanks.count {
            var fillInBlanks = arrFillInBlanks[i]
            if fillInBlanks.sequence != nil {
                fillInBlanks.word = fillInBlanks.correctAns
                arrFillInBlanks[i] = fillInBlanks
            }
        }
        collectionViewFillInBlanks.reloadDataInMain()
    }
    
    //MARK:- show timer label and button
    func updateTimer(text: String) {
        print("updateTimer ListningFillInBlanksVC \(text)")
        if mockQestionModel != nil {
            timeOnScreen += 1
            if screenStatus == .begining {
                viewSkipOrSaveHeightConstraint.constant = 50
                viewSkipOrSave.isHidden = false
                waitTime -= 1
                if waitTime == 1 {
                    screenStatus = .playingAudio
                    playAudio()
                }
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
                btnSkipOrSave.isHidden = false
                self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
               // viewSkipOrSave.isHidden = false
            } else if screenStatus == .playingAudio {
                viewSkipOrSaveHeightConstraint.constant = 0
                viewSkipOrSaveHeightConstraint.constant = 0
                viewSkipOrSave.isHidden = true
            } else {
                viewSkipOrSaveHeightConstraint.constant = 0
            }
        } else {
            if screenStatus == .begining {
                print("Beginning in \(text) seconds")
                lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: text, thirdStr: " seconds")
                viewSkipOrSaveHeightConstraint.constant = 50
                self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
                viewSkipOrSave.isHidden = false
                btnSkipOrSave.isHidden = false
                if text == "1" {
                    screenStatus = .playingAudio
                    playAudio()
                    viewSkipOrSaveHeightConstraint.constant = 0
                    viewSkipOrSave.isHidden = true
                }
            } else if screenStatus == .playingAudio {
                print("Playing \(text) seconds")
                viewSkipOrSaveHeightConstraint.constant = 0
                viewSkipOrSave.isHidden = true
                btnSkipOrSave.isHidden = true
            } else {
                viewSkipOrSaveHeightConstraint.constant = 0
            }
        }
    }
    
    //MARK:- set data on view
    func setupCollectionView() {
        if let mockQestionModel = self.mockQestionModel {
            let strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(strSeprated)
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "" )
                    // var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "", id: strSeprated[i])
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        if sequence == strSeprated[i] {
                           
                            print(strSeprated[i])
                            fillInTheBlanks.word = ""
                            fillInTheBlanks.isEditable = true
                            fillInTheBlanks.sequence = j+1
                            fillInTheBlanks.placeholder = "  _  \(j+1)  _  "
                            fillInTheBlanks.correctAns = arrOptionModel[j].option.validate /*strSeprated[i]*/

                        }
                    }
                    arrFillInBlanks.append(fillInTheBlanks)
                }
            }
            collectionViewFillInBlanks.reloadDataInMain()
        } else if let mockQestionModel = self.practiceTestModel {
            let strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(strSeprated)
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "")
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        if sequence == strSeprated[i] {
                            
                            print(strSeprated[i])
                            fillInTheBlanks.word = ""
                            fillInTheBlanks.isEditable = true
                            fillInTheBlanks.sequence = j+1
                            fillInTheBlanks.placeholder = "  _  \(j+1)  _  "
                            fillInTheBlanks.correctAns = arrOptionModel[j].option.validate /*strSeprated[i]*/
                            
                        }
                    }
                    arrFillInBlanks.append(fillInTheBlanks)
                }
            }
            collectionViewFillInBlanks.reloadDataInMain()
        }
    }
    
    
    //MARK:- ans api call
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
        
            var userText = ""
            for fillInBlanks in arrFillInBlanks {
                if let sequence = fillInBlanks.sequence {
                    userText += (fillInBlanks.word.count > 0 ? fillInBlanks.word : "{\(sequence)}") + " "
                } else {
                    userText += fillInBlanks.word + " "
                }
            }
            print("userText >>>>> \(userText)")
            let filteredArray = arrFillInBlanks.filter({ $0.sequence != nil }).sorted { (obj1, obj2) -> Bool in
                return obj1.sequence! < obj2.sequence!
            }
        
            let optionid = filteredArray.map({ $0.word.count > 0 ? $0.word : "null" }).joined(separator: ", ")
            print("optionid >>>>> \(optionid)")
        
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            
            let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(Int(sectionTime))" , "usetxt": "\(userText)" , "word_count": "" , "optionid": "\(optionid)"]
            print("parameter >>>> \(parameter)")
            
            testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
            
            
        }
    }
    
    //MARK:- btn skip action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        viewSkipOrSaveHeightConstraint.constant = 0
        viewSkipOrSave.isHidden = true
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }else if let practiceDetailVC = self.practiceDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
}


extension ListningFillInBlanksVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateLeftAlignedLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFillInBlanks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fillInTheBlanksModel = arrFillInBlanks[indexPath.row]
        if fillInTheBlanksModel.isEditable {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FillInBlanksCollectionCell", for: indexPath) as! FillInBlanksCollectionCell
            if let sequence = arrFillInBlanks[indexPath.row].sequence {
                cell.textField.tag = sequence
                cell.textField.delegate = self
                cell.textField.placeholder = arrFillInBlanks[indexPath.row].placeholder
                cell.textField.text = arrFillInBlanks[indexPath.row].word
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
            cell.setupData(text: arrFillInBlanks[indexPath.row].word)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 25.0)
        if arrFillInBlanks.indices.contains(indexPath.row) {
            size.width = arrFillInBlanks[indexPath.row].wordWidth
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}

extension ListningFillInBlanksVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tag = textField.tag
        if let index = arrFillInBlanks.index(where: { (fillInBlanksModel) -> Bool in
            return fillInBlanksModel.sequence == tag
        }) {
            arrFillInBlanks[index].word = textField.text.validate
        }
    }
    
}

//MARK:- Audio
extension ListningFillInBlanksVC {
    
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD("Prepare audio", onView: self.view)
            avPlayerItem = AVPlayerItem(url: url)
            avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
            audioPlayer = AVPlayer(playerItem: avPlayerItem)
            audioPlayer?.pause()
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            audioPlayer?.play()
            let interval = CMTime(value: 1, timescale: 2)
            audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                let seconds = CMTimeGetSeconds(progressTime)
                if let duration = self.audioPlayer?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let progress = Float(seconds / durationSeconds)
                    self.audioProgress.progress = progress
                    if progress >= 1 {
                        self.pauseAndStartTimer(false)
                        self.updateScreenWhenAudioStops()
                    }
                }
            })
        }
    }
    
    func stopPlaying() {
        audioPlayer?.pause()
        audioPlayer = nil
        avPlayerItem = nil
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            pauseAndStartTimer(false)
            screenStatus = .begining
            appDelegate.hideHUD(self.view)
            avPlayerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: &playerItemContext)
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
    
}


struct FillInBlanksModel {
    var word: String {
        didSet {
            if !isEditable {
                self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
            }
        }
    }
    var isEditable: Bool {
        didSet {
            if isEditable {
                self.wordWidth = 120
            }
        }
    }
    var wordWidth: CGFloat = 0.0
    var sequence: Int?
    var placeholder: String
    var correctAns: String
    var displayCorrectAns = false
    var id: String
}

extension FillInBlanksModel {
    
    init(word: String, isEditable: Bool, sequence: Int?, placeholder: String, correctAns: String) {
        self.word = word
        self.isEditable = isEditable
        self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        // print("wordWidth---\(wordWidth)")
        self.sequence = sequence
        self.placeholder = placeholder
        self.correctAns = correctAns
        self.id = ""        
    }
    
    init(word: String, isEditable: Bool, sequence: Int?, placeholder: String, correctAns: String, id: String) {
        self.word = word
        self.isEditable = isEditable
        self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        self.sequence = sequence
        self.placeholder = placeholder
        self.correctAns = correctAns
        self.id = id
    }
    
}

