//
//  SummerySpokenVC.swift
//  PTEMaster
//
//  Created by mac on 05/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

class SummerySpokenVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
    
    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    var screenStatus = ScreenStatus.preparingToPlay
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var arrOptionModel = [OptionsModel]()
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    
    
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        textView.delegate = self
        setupData()
    }
    
    //stop audio
    override func viewDidDisappear(_ animated: Bool) {
        stopPlaying()
    }
    
    //MARK:- set data on summery view
    func setupData() {
        
       /* audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 8)
        audioProgress.clipsToBounds = true
        self.audioProgress.layer.cornerRadius = 10*/
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
        
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }else if let mockQestionModel = practiceTestModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
           // self.audioProgress.layer.cornerRadius = 15.0

            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }
    }
  
    func updateTimer(text: String) {
        // print(" update total time \(text)")
        timeOnScreen += 1
        if screenStatus == .begining {
            constBottomViewHeight.constant = 50
            waitTime -= 1
            
            if waitTime == 0 {
                screenStatus = .playingAudio
                playAudio()
                
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
            
            btnSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
        }else if screenStatus == .playingAudio {
            
            print("***********playingAudio**********")
            self.constBottomViewHeight.constant = 0
            
            self.viewSkipOrSave.isHidden = true
            
        } else {
            // print("else block ")
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }
    
    
    //MARK:- ans api
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
            let ansWordCount = textView.text.removeHtmlFromString().components(separatedBy: " ")
            print(" >>> \(ansWordCount)")
            print(ansWordCount)
            // total remaning time me se jitna ho gya h utna - krke jo bcha h wo dena h
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(sectionTime)" , "usetxt": "\(textView.text!)" , "word_count": "\(ansWordCount.count)" , "optionid": ""]
            testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
        } else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        view.layoutSubviews()
    }
    
    //MARK:- btn skip action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .begining {
            constBottomViewHeight.constant = 0
            viewSkipOrSave.isHidden = true
            screenStatus = .playingAudio
            DispatchQueue.main.async {
                self.playAudio()
            }
        }
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
}


//MARK:- Audio
extension SummerySpokenVC {
    
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD("Prepare audio", onView: self.view)
            avPlayerItem = AVPlayerItem(url: url)
            avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
            audioPlayer = AVPlayer(playerItem: avPlayerItem)
            audioPlayer?.pause()
        }
    }

    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            // constBottomViewHeight.constant = 0
            audioPlayer?.play()
            let interval = CMTime(value: 1, timescale: 2)
            audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                let seconds = CMTimeGetSeconds(progressTime)
                if let duration = self.audioPlayer?.currentItem?.duration {
                    let durationSeconds = CMTimeGetSeconds(duration)
                    let progress = Float(seconds / durationSeconds)
                    self.audioProgress.progress = progress
                    if progress >= 1 {
                        self.pauseAndStartTimer(false)
                        self.updateScreenWhenAudioStops()
                    }
                }
            })
        }
    }
    
    func stopPlaying() {
        audioPlayer?.pause()
        audioPlayer = nil
        avPlayerItem = nil
    }
    
    func updateScreenWhenAudioStops() {
        stopPlaying()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            pauseAndStartTimer(false)
            screenStatus = .begining
            appDelegate.hideHUD(self.view)
            avPlayerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: &playerItemContext)
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
}



