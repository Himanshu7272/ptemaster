//
//  WritingEasyVC.swift
//  PTEMaster
//
//  Created by mac on 28/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class WritingEasyVC: UIViewController , UITextViewDelegate , UIScrollViewDelegate  {
    @IBOutlet weak var ContHeightViewCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionViewResult: UICollectionView!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimerSecond: UILabel!
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var viewCollectionResult: UIView!
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    
    var timeLeft: TimeInterval = 60
    var timeOnScreen: TimeInterval = 0
    var countDownTimer: Timer?
    var arrResult = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ContHeightViewCollection.constant = 0
        viewCollectionResult.isHidden = true
        textView.delegate = self
        setupData()
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }
    }
    
    func updateTimer(text: String) {
        
        // print(text)
        timeOnScreen += 1
        let final: NSMutableAttributedString = NSMutableAttributedString()
        
        let first = NSAttributedString(string: "Time Left", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        let second = NSAttributedString (string: " \(text)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.blue])
        
        let third = NSAttributedString(string: " seconds", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        final.append(first)
        final.append(second)
        final.append(third)
        lblTimerSecond.attributedText = final
        
    }
    
    func checkAns() {
        self.callAnsAPI()
    }
    

    
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        scrlView.scrollToBottom(animated: true)
        // view.layoutSubviews()  // view.layoutIfNeeded()   //aRC
        view.layoutIfNeeded()
    }
    
    @IBAction func actionRetry(_ sender: Any) {
        
        if let testDetailVC = self.parent as? PracticeDetailVC {
            testDetailVC.callRetryAgain()
        }
    }
    
    
}

extension WritingEasyVC{
    //MARK:- ans api
    func callAnsAPI(){
         appDelegate.showHUD(appDelegate.strLoader, onView: view)
        if let _ = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
            let ansWordCount = textView.text.count //textView.text.removeHtmlFromString().components(separatedBy: " ")

            
            let parameter: [String: Any] = ["question_cat":questionModal.categoryId! as Any, "word_count": "\(ansWordCount)" ,"question_id": "\(questionModal.id.validate)","userid": "\(AppDataManager.shared.loginData.id!)" , "user_string": "\(textView.text!)"  , "mocqdesc": questionModal.answer! ]
            print("params --\(parameter)-")
       IPServiceHelper.shared.callAnswerAPIWithParameters(parameter, ansApi:BaseUrl +  RequestPath.practice_result_writing.rawValue) { (result, responseCode, error) in
            ///Success Case
            if responseCode ==  200
            {
                DispatchQueue.main.async(execute: {
                    appDelegate.hideHUD(self.view)
                    self.arrResult.removeAll()
                    if let dict = result{
                         var arrList = [[String:Any]]()
                         arrList.append(["finalResult": (dict["grammar"] as Any)])
                         arrList.append(["finalResult": (dict["context"] as Any)])
                        arrList.append(["finalResult": (dict["form"] as Any)])
                        arrList.append(["finalResult": (dict["context"] as Any)])
                        arrList.append(["finalResult": (dict["marks"] as Any)])
                        self.arrResult = arrList
                    }
                    
                    self.ContHeightViewCollection.constant = 140
                    self.viewCollectionResult.isHidden = false
                    self.collectionViewResult.reloadData()
                    if let testDetailVC = self.parent as? PracticeDetailVC{
                       testDetailVC.stopTimer()
                     }
                })
            }else{
                appDelegate.hideHUD(self.view)
        }
        }
    }
    }
}

extension WritingEasyVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        
         cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["finalResult"] as? Double ?? 0.0
        let strFinalResult = String(format: "%.1f", strResult)
       // let fullArr = strFinalResult.components(separatedBy: ".")
     //   let first =  fullArr[0]
       // let last =  fullArr[1]
        if indexPath.row == 0 {
            cell.lblTitle.text = "Grammer"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
    }
         else if indexPath.row == 1 {
            cell.lblTitle.text = "Content"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
        } else if indexPath.row == 2 {
            cell.lblTitle.text = "Form"
            cell.viewbg.layer.borderColor = UIColor.brown.cgColor
            cell.lblResult.text = strFinalResult + "/" + "1"
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Vocabulary"
            cell.viewbg.layer.borderColor = UIColor.yellow.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
        }else if indexPath.row == 4 {
            cell.lblTitle.text = "Final"
            cell.viewbg.layer.borderColor = UIColor.red.cgColor
            cell.lblResult.text = strFinalResult + "/" + "90"
        }
        return cell
    }
    
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    
    
}
