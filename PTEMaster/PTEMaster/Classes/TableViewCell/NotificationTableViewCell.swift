//
//  NotificationTableViewCell.swift
//  PTEMaster
//
//  Created by CTIMac on 13/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell
{

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTest: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
