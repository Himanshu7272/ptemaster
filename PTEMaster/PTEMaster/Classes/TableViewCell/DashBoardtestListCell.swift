//
//  DashBoardtestListCell.swift
//  PTEMaster
//
//  Created by mac on 22/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class DashBoardtestListCell: UITableViewCell {

    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTest: UIImageView!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(info: DashBordTestListModel) {
        
        lblDescription.text = info.short_desc.validate == "" ? "N/A" : info.short_desc.validate
        lblTitle.text = info.name.validate == "" ? "N/A" : info.name.validate
        
        if let url = URL(string: BaseUrlTestImg + info.image.validate) {
            
            imgTest.kf.indicatorType = .activity
            imgTest.kf.setImage(with: url) { result in
                switch result {
                case .success(let value):
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }
        
        if info.status.validate == "0" {          // buy now $30 aud
           lblDate.text = "  BUY NOW "
           lblPrice.text = "$\(info.price.validate) AUD  "
        } else if info.status.validate == "1" {   //subscribe
           lblDate.text = "  SUBSCRIBE  "
           lblPrice.text = ""
        } else if info.status.validate == "2" {   //start test
            lblDate.text = "  START TEST  "
            lblPrice.text = ""
        }
        
        
        
    }

}
