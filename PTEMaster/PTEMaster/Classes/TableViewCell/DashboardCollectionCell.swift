//
//  DashboardCollectionCell.swift
//  PTEMaster
//
//  Created by CTIMac on 19/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class DashboardCollectionCell: UICollectionViewCell {
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
