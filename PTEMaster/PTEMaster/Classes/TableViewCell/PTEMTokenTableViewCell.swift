
//
//  PTEMTokenTableViewCell.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation

protocol PTEMTokenDelegate:NSObject {
    func actionBuyNow(cell:PTEMTokenTableViewCell)
}

class PTEMTokenTableViewCell: UITableViewCell {
    @IBOutlet weak var lblstudentName: UILabel!
    @IBOutlet weak var lblCreditToken: UILabel!
    @IBOutlet weak var lblAUD: UILabel!
    @IBOutlet weak var imgIcons: UIImageView!
    @IBOutlet weak var btnBuyNow: GradientButton!
    weak var delegate:PTEMTokenDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func actionsBuy(_ sender: Any) {
        delegate?.actionBuyNow(cell: self)
    }
    
    
    
    func isSetData(dict:TokenModal) {
        lblstudentName.text = dict.tk_pkg_name
        if  dict.tk_pkg_active_time != "0" {
            let tk_pkg_active_time = dict.tk_pkg_active_time
            lblCreditToken.text = "Time Token : " + tk_pkg_active_time!
        } else {
         //  let tk_pkg_active_time = dict.tk_pkg_active_time
            lblCreditToken.text = "Credit Token : " + dict.tk_pkg_credits!
        }
        lblAUD.text = "AUD " + dict.tk_pkg_price!
       // imgIcons.image =  dict.
    }
}
