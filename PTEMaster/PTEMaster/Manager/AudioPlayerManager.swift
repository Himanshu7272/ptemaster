//
//  AudioPlayerManager.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 23/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

class AudioPlayerManager: NSObject {

    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    var timeObserver: Any?
    
    var audioReadyToPlay: (() -> Void)?
    var audioProgress: ((Float) -> Void)?
    
    var isPlaying: Bool {
        return audioPlayer?.isPlaying ?? false
    }
    
    override init() {
        super.init()
        initialSetup()
    }
    
    private func initialSetup() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            if let audioReadyToPlay = self.audioReadyToPlay {
                audioReadyToPlay()
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                break
            case .failed:
                // Player item failed. See error.
                break
            case .unknown:
                // Player item is not yet ready.
                break
            }
        }
    }
    
    func prepareAudio(from url: URL, audioReadyToPlay: @escaping () -> Void) {
        self.audioReadyToPlay = audioReadyToPlay
        avPlayerItem = AVPlayerItem(url: url)
        avPlayerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
        audioPlayer = AVPlayer(playerItem: avPlayerItem)
        audioPlayer?.pause()
    }
    
    func playAudio(_ audioProgress: @escaping (Float) -> Void) {
        self.audioProgress = audioProgress
        audioPlayer?.play()
        let interval = CMTime(value: 1, timescale: 2)
        self.timeObserver = audioPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
            let seconds = CMTimeGetSeconds(progressTime)
            if let duration = self.audioPlayer?.currentItem?.duration {
                let durationSeconds = CMTimeGetSeconds(duration)
                let progress = Float(seconds / durationSeconds)
                audioProgress(progress)
                if progress >= 1, let timeObserver = self.timeObserver {
                    self.audioPlayer?.removeTimeObserver(timeObserver)
                }
            }
        })
    }
    
    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayer?.pause()
        if destroyPlayer {
            audioPlayer = nil
            avPlayerItem = nil
        }
    }
    
    func replay(_ audioProgress: @escaping (Float) -> Void) {
        initialSetup() 
        audioPlayer?.seek(to: CMTime.zero)
        playAudio(audioProgress)
    }
    
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

//AVAudioPlayerDelegate
