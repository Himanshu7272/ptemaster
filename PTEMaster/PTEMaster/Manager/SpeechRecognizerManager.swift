//
//  SpeechRecognizerManager.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 23/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech

class SpeechRecognizerManager: NSObject {
    
    enum State {
        case ideal
        case recognizing
        case paused
        case stoped
    }
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    var state = State.ideal
    var timer: Timer?
    
    var stopRecognizingAfter5Sec = false
    let timeInterval: TimeInterval = 5
    var recognizingStopAfter5SecBlock: (() -> Void)?
    
    var isAuthorized: Bool {
        return SFSpeechRecognizer.authorizationStatus() == .authorized
    }
    var isRunning: Bool {
        return audioEngine.isRunning
    }
    
    override init() {
        super.init()
        initialSetup()
    }
    
    private func initialSetup() {
        speechRecognizer.delegate = self
    }
    
    func checkForSpeechRecognizer() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            break
        case .denied:
            print("User denied access to speech recognition")
        case .restricted:
            print("Speech recognition restricted on this device")
        case .notDetermined:
            askForSpeechRecognizer()
            print("Speech recognition not yet authorized")
        }
    }
    
    private func askForSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            self.checkForSpeechRecognizer()
        }
    }
    
    private func setupAudioSession() {
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
    }
    
    func startRecording(_ textUpdate: @escaping (String) -> Void) {
        
        startTimer()
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        setupAudioSession()
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        let inputNode = audioEngine.inputNode //4
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            var isFinal = false  //8
            if result != nil {
                self.startTimer()
                let text = result!.bestTranscription.formattedString
                textUpdate(text)
                isFinal = result!.isFinal
            }
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.stopTimer()
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
            state = .recognizing
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func pauseRecording() {
        if audioEngine.isRunning {
            audioEngine.pause()
            state = .paused
        }
    }
    
    func reStartRecording() {
        do {
            try audioEngine.start()
            state = .recognizing
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func stopRecording() {
        if audioEngine.isRunning {
            audioEngine.stop()
            state = .stoped
            recognitionRequest?.endAudio()
        }
    }
    
    private func startTimer() {
        if stopRecognizingAfter5Sec {
            stopTimer()
            self.timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { (_) in
                self.pauseRecording()
                self.stopTimer()
                if let recognizingStopAfter5SecBlock = self.recognizingStopAfter5SecBlock {
                    recognizingStopAfter5SecBlock()
                }
            })
        }
    }
    
    private func stopTimer() {
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }

}


extension SpeechRecognizerManager: SFSpeechRecognizerDelegate {
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphoneButton.isEnabled = true
        } else {
            //microphoneButton.isEnabled = false
        }
    }
}
