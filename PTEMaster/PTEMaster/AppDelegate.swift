//
//  AppDelegate.swift
//  PTEMaster
//
//  Created by CTIMac on 11/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var reachability = Reachability()!

    var isNetworkAvailable: Bool = false
    var strLoader = ""
    var strDeviceToken: String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        registerForPushNotifications()
        setupNetworkMonitoring()
        IQKeyboardManager.shared.enable = true;
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        application.statusBarStyle = .lightContent // .default
        // Override point for customization after application launch.
        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: -- PushNoification
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        //log.info("Device Token String:\(token)")
        
        //** Remove special character and get valid device token string
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        
        let deviceTokenString: String = (token as NSString ) .trimmingCharacters(in: characterSet) .replacingOccurrences(of: " ", with: "" ) as String
        
        strDeviceToken = deviceTokenString
        UserDefaults.standard.setValue(strDeviceToken, forKey: "device_token")
        UserDefaults.standard.setValue(strDeviceToken, forKey: "device_id")
        
        //UserDefaults.standard.set(Util.getValidString(LoggedInUser.sharedUser.deviceToken), forKey: "device_token")
        
        let isLogin = UserDefaults.standard.object(forKey: "LoggedIn") as? String
        
        if isLogin == "true" {
            // strUserId = "\(String(describing: UserDefaults.standard.object(forKey: "UserId") as! NSNumber))"
            //NotificationAPI()
        } else {
            //strUserId = ""
            //NotificationAPI()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }

    //****************************************************
    // MARK: MBProgressHUD Methods
    //****************************************************
    
    func showHUD(_ hudTitle : String, onView: UIView) {
        
//        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: onView, animated: true)
//        //hud.mode = MBProgressHUDMode.AnnularDeterminate
//        hud.bezelView.color = UIColor.clear
//        hud.contentColor = UIColor.white
//        hud.backgroundColor  = UIColor.black.withAlphaComponent(0.5)
//        //hud.activityIndicatorColor = colorWhite
//
//        hud.label.textColor = UIColor.white
//        hud.bezelView.color = UIColor.lightGray
//        hud.label.font = UIFont(name: "", size: 17)
        
       /* if Util.isValidString(hudTitle) {
            hud.label.text = hudTitle
        }
        else {
            hud.label.text = strLoader
        }*/
         Loader.showLoader(hudTitle)
    }
    
    func hideHUD(_ fromView: UIView) {
       // MBProgressHUD.hide(for: fromView, animated: true)
        Loader.hideLoader()
    }
    
    //****************************************************
    // MARK: - Network Reachability Methods
    //****************************************************
    
    
    //MARK:- Reachability
    func setupNetworkMonitoring() {
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
                self.isNetworkAvailable = true
            }
        }
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                print("Not reachable")
                self.isNetworkAvailable = false
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            self.isNetworkAvailable = true
        } else {
            print("Network not reachable")
            self.isNetworkAvailable = false
        }
    }

}

